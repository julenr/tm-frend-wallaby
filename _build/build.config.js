"use strict";

function yarnInstall() {
    const OS = require('os');
    const { execSync } = require('child_process');

    const CMD_EXT = OS.platform().indexOf('win32') !== -1 ? '.cmd' : '';
    execSync(`yarn${CMD_EXT} install`, { stdio: 'inherit' });
}

function yarnAdd(packageName) {
    const OS = require('os');
    const { execSync } = require('child_process');

    const CMD_EXT = OS.platform().indexOf('win32') !== -1 ? '.cmd' : '';
    execSync(`yarn${CMD_EXT} add ${packageName}`, { stdio: 'inherit' });
}

const STATUS_MESSAGES = {
    lint: {
        start: 'Lint starting ...',
        success: 'Lint done',
        fail: 'Lint failed'
    },
    test: {
        start: 'Karma starting ...',
        success: 'Karma done',
        fail: 'Karma failed'
    },
    build: {
        start: 'Webpack starting ...',
        success: 'You made a new FrEnd',
        fail: 'You did not make a new FrEnd'
    },
    coverage: {
        start: 'Publishing coverage report ...', success: 'Coverage report published'
    },
    bundle: {
        start: 'Publishing bundle report ...', success: 'Bundle report published'
    }
};

function createEventFilter(rootPath) {
    const path = require('path');

    rootPath = path.normalize(rootPath);
    return [
        (message) => message.replace(rootPath, '.')
    ];
}

const TEST_EVENT_FILTER = [
    (message) => {
        if (/Error during cleanup of component/.test(message))
            throw new Error(message);
        return true;
    },
    (message) => {
        if (/is deprecated and will be removed from the public API in a future version of sinon/.test(message))
            throw new Error(message);
        return true;
    }
];

function printBanner() {
    return `
 _____              _        __  __
|_   _| __ __ _  __| | ___  |  \\/  | ___    __ _
  | || '__/ _\` |/ _\` |/ _ \\ | |\\/| |/ _ \\  /  ('>--
  | || | | (_| | (_| |  __/ | |  | |  __/  \\__/
  |_||_|  \\__,_|\\__,_|\\___| |_|  |_|\\___|   L\\_
  L i f e     l i v e s     h e r e

Node: ${process.version}`;
}

function printSuccess(outDir) {
    let message = outDir ? `Files were copied to ${outDir}` : 'no outDir was specified so nothing was copied';
    return `
Build done - ${message}
-----------------------------------------------------------`;
}

function getArg(argName) {
    let args = process.argv.slice(2);
    if(args.length) {
        for (let i = 0; i < args.length; i++) {
            let arg = args[i];
            if(arg && arg.toLowerCase().indexOf(argName) > -1) {
                let splitArg = arg.split('=');
                if(splitArg.length === 2) {
                    return splitArg[1];
                }
            }
        }
    }
    return undefined;
}

module.exports = {
    yarnInstall,
    yarnAdd,
    STATUS_MESSAGES,
    createEventFilter,
    TEST_EVENT_FILTER,
    printBanner,
    printSuccess,
    getArg
};
