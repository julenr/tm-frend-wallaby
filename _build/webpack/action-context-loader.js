const path = require('path');
const loaderUtils = require('loader-utils');

module.exports = function actionContextLoader (source) {
    if (this.cacheable) {
        this.cacheable();
    }
    const options = loaderUtils.getOptions(this);
    const relativeTo = options.relativeTo || '.';

    const context = '[' + path.relative(relativeTo, path.dirname(this.resourcePath)).replace(/\\/g, '/') + ']';
    const regex = /export class (\w+) extends ActionBase\(\) {/g;

    return source.replace(regex, (match, actionName) => match.replace('ActionBase()', `ActionBase('${context} ${actionName}')`));
};
