const ContextElementDependency = require('webpack/lib/dependencies/ContextElementDependency');
const glob = require('glob');
const path = require('path');
const asyncLib = require("async");
const fs = require('fs-extra');

function FocusTestWebpackPlugin(options) {
    let opt = options || {};
    this.enabled = opt.enabled;
    if (opt.pathFilter && opt.pathFilter.trim().length > 0) {
        this.pathFilter = opt.pathFilter.toLowerCase().replace(/\\/g, '/');
    }
}

FocusTestWebpackPlugin.prototype.findFocusFiles = function(basePath, pathFilter) {
    let focusFiles = [];
    let regex = new RegExp('fdescribe');
    glob.sync(path.join(basePath, '/**/*.spec.ts')).forEach(filePath => {
        if (pathFilter && filePath.toLowerCase().replace(/\\/g, '/').indexOf(this.pathFilter) === -1)
            return;
        let data = fs.readFileSync(filePath);
        // test if file includes a fdescribe
        if (!regex.test(data))
            return;
        // remove /* */ and // from the file contents.
        let contentsWithNoComments = data.toString().replace(/\/\*[\s\S]+?\*\/|\/\/.*/g, '');
        // re-run fdescribe test with no comments. if it fails, the fdescribe was commented out
        if (!regex.test(contentsWithNoComments))
            return;
        let relativePath = path.relative(basePath, filePath).replace(/\\/g, '/');
        if (relativePath[0] !== '.')
            relativePath = `./${relativePath}`;
        focusFiles.push(relativePath);
    });
    return focusFiles;
};

FocusTestWebpackPlugin.prototype.apply = function(compiler) {
    let enabled = this.enabled;
    let pathFilter = this.pathFilter;
    compiler.plugin('context-module-factory', cmf => {
        cmf.plugin('after-resolve', (result, callback) => {
            if (!result) {
                return callback();
            }
            if (enabled === false || !result.regExp || !result.recursive || /node_modules/.test(result.resource)) {
                return callback(null, result);
            }

            let focusFiles = this.findFocusFiles(result.resource, pathFilter);
            if (focusFiles.length > 0) {
                console.log(`\n[spec] test limited to ${focusFiles.length} files. remove fdescribe to run all tests.`);
                focusFiles.forEach(file => {
                    console.log(`[spec] ${file}`);
                });
            }

            if (focusFiles.length === 0) {
                return callback(null, result);
            }

            result.resolveDependencies = (fs, resource, recursive, regExp, callback) => {
                if(!regExp || !resource)
                    return callback(null, []);
                (function addDirectory(directory, callback) {
                    fs.readdir(directory, function(err, files) {
                        if(err) return callback(err);
                        if(!files || files.length === 0) return callback(null, []);
                        asyncLib.map(files.filter(function(p) {
                            return p.indexOf(".") !== 0;
                        }), function(seqment, callback) {

                            const subResource = path.join(directory, seqment);

                            fs.stat(subResource, function(err, stat) {
                                if(err) return callback(err);

                                if(stat.isDirectory()) {

                                    if(!recursive) return callback();
                                    addDirectory.call(this, subResource, callback);

                                } else if(stat.isFile()) {

                                    const obj = {
                                        context: resource,
                                        request: "." + subResource.substr(resource.length).replace(/\\/g, "/")
                                    };

                                    let includeFile = focusFiles.length === 0 || focusFiles.some(f => obj.request.indexOf(f) !== -1);

                                    if (includeFile) {
                                        alternatives = [obj].filter(function(obj) {
                                            return regExp.test(obj.request);
                                        }).map(function(obj) {
                                            const dep = new ContextElementDependency(obj.request);
                                            dep.optional = true;
                                            return dep;
                                        });
                                        callback(null, alternatives);
                                    } else {
                                        callback();
                                    }
                                } else callback();

                            }.bind(this));

                        }.bind(this), (err, result) => {
                            if(err) return callback(err);

                            if(!result) return callback(null, []);

                            callback(null, result.filter(function(i) {
                                return !!i;
                            }).reduce(function(a, i) {
                                return a.concat(i);
                            }, []));
                        });
                    }.bind(this));
                }.call(this, resource, callback));
            };
            return callback(null, result);
        });
    });
};

module.exports = FocusTestWebpackPlugin;
