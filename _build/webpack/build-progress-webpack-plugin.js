const { ProgressPlugin } = require('webpack');

class BuildProgressWebpackPlugin {
    constructor() {
        this.lastState = null;
        this.lastStateTime = null;
        this.lastPercentage = -1;
        this.logStates = ['compiling', 'chunk asset optimization', 'emitting']; // + building modules
        this.percentageStep = 5; // only show updates if percentage has moved this far. (0.05 = 5%)
        this.moduleProgess = '';
        this.minimumStateTime = 100; // by default we are only interested in steps that take more than 100ms
    }

    formatPercentage(percentage) {
        let result = `${percentage}%`;
        if (percentage < 100) {
            result = ` ${result}`;
        }
        if (percentage < 10) {
            result = ` ${result}`;
        }
        return result;
    }

    progressHandler(percentage, message) {
        let state = message;
        percentage = Math.floor(percentage * 100);

        // log build progress
        if (state.indexOf("building modules") >= 0) {
            const details = Array.prototype.slice.call(arguments, 2);
            this.moduleProgess = details[0];
            if ((percentage - this.lastPercentage) < this.percentageStep)
                return;
            this.lastPercentage = percentage;

            let message = `${this.formatPercentage(percentage)} ${state || ''}`;
            details.forEach(detail => {
                if (!detail)
                    return;
                if (detail.length > 50)
                    detail = `...${detail.substr(detail.length - 47)}`;
                message += ` ${detail}`;
            });
            console.log(message);
        }

        // log progress
        state = state.replace(/^\d+\/\d+\s+/, "");
        if (percentage === 0) {
            this.lastState = null;
            this.lastStateTime = Date.now();
            this.moduleProgess = '';
            console.log(`  0% ${message}`);
        } else if (percentage === 100) {
            console.log(`100% done (${this.moduleProgess})`);
        } else if (state !== this.lastState) {
            const now = Date.now();
            // only log states we are interested in. typically those that take a lot of time
            if (this.lastState) {
                let timeTaken = now - this.lastStateTime;
                if (( timeTaken > this.minimumStateTime ) || this.logStates.indexOf(this.lastState) > -1) {
                    console.log(` ${timeTaken}ms (${this.lastState})`);
                }
            }
            if (this.logStates.indexOf(state) > -1) {
                console.log(`${this.formatPercentage(percentage)} ${message}`);
            }
            this.lastState = state;
            this.lastStateTime = now;
        }
    }

    apply(compiler) {
        const progressPlugin = new ProgressPlugin(this.progressHandler.bind(this));
        progressPlugin.apply(compiler);
    };
}

module.exports = BuildProgressWebpackPlugin;
