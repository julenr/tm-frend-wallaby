const fs = require('fs');
const childProcess = require('child_process');

if (!fs.existsSync('./dll/polyfill.dll.js') || !fs.existsSync('./dll/vendor.dll.js')) {
    console.log(`
    Building Dlls ...
    `);

    return childProcess.execSync('npm run build:dll', { stdio: [0,1,2] });
} else {
    console.log(`
    Dlls exist, skipping dll build.
    `);
}
