"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Lint = require("tslint");
const Ts = require("typescript");
const RULE_NAME = 'no-imports-from-module-blacklist';
class Rule extends Lint.Rules.AbstractRule {
    apply(sourceFile) {
        // NOTE: Input arguments are a list of case-insensitive regular expressions
        let args = this.getOptions().ruleArguments;
        if (!args || !args.length) {
            throw new Error(`${RULE_NAME}: No module patterns defined in tslint.json!`);
        }
        let bannedModulePatterns = args.map(arg => new RegExp(arg, 'i'));
        return this.applyWithWalker(new NoDirectImportsFromWalker(sourceFile, RULE_NAME, { bannedModulePatterns }));
    }
}
exports.Rule = Rule;
class NoDirectImportsFromWalker extends Lint.AbstractWalker {
    walk(sourceFile) {
        let importsVisitor = (node) => {
            if (node.kind === Ts.SyntaxKind.ImportDeclaration) {
                this.scanImportDeclaration(node);
                return;
            }
            Ts.forEachChild(node, importsVisitor);
        };
        Ts.forEachChild(sourceFile, importsVisitor);
    }
    scanImportDeclaration(importDeclaration) {
        let moduleSpecifier = asStringLiteral(importDeclaration.moduleSpecifier);
        if (!moduleSpecifier) {
            throw new Error(`${RULE_NAME}: Invalid module specifier: ${importDeclaration.moduleSpecifier.getText()}`);
        }
        for (let pattern of this.options.bannedModulePatterns) {
            if (pattern.test(moduleSpecifier.text)) {
                let error = `Imports from '${moduleSpecifier.text}' are not allowed! Can you import from a more specific module?`;
                this.addFailureAtNode(moduleSpecifier, error);
            }
        }
    }
}
function asStringLiteral(node) {
    return node && node.kind === Ts.SyntaxKind.StringLiteral ? node : null;
}
