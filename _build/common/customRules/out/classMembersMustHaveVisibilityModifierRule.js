"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Lint = require("tslint");
const Ts = require("typescript");
class Rule extends Lint.Rules.AbstractRule {
    apply(sourceFile) {
        return this.applyWithWalker(new ClassMembersMustHaveVisibilityModifier(sourceFile, this.getOptions()));
    }
}
exports.Rule = Rule;
class ClassMembersMustHaveVisibilityModifier extends Lint.RuleWalker {
    visitPropertyDeclaration(node) {
        // Bare string, number or boolean literal "properties" are a common pattern and allowed.
        // E.g.
        //    class Foo {
        //      @HostBinding('class.foo-base')
        //      true;
        //    }
        let isNamePrimitiveLiteral = node.name.kind === Ts.SyntaxKind.StringLiteral ||
            node.name.kind === Ts.SyntaxKind.NumericLiteral ||
            node.name.kind === Ts.SyntaxKind.Identifier && (node.name.originalKeywordKind === Ts.SyntaxKind.TrueKeyword ||
                node.name.originalKeywordKind === Ts.SyntaxKind.FalseKeyword);
        let propertyHasTypeDeclarationOrInitExpression = !!node.type || !!node.initializer;
        // Enforce the visibility modifier only if:
        //   a) the property name does not look like a primitive literal, OR
        //   b) the property has a type notation or initializer expression
        //      (if we are using keywords as property names then we ought to have a public/private modifier anyway)
        let shouldCheckForPublicModifier = !isNamePrimitiveLiteral || propertyHasTypeDeclarationOrInitExpression;
        if (shouldCheckForPublicModifier) {
            if (this.checkShouldHaveVisibilityModifier(node)) {
                this.addFailureClassMemberVisibilityModifierMissing(node);
            }
        }
        super.visitPropertyDeclaration(node);
    }
    visitMethodDeclaration(node) {
        if (this.checkShouldHaveVisibilityModifier(node)) {
            this.addFailureClassMemberVisibilityModifierMissing(node);
        }
        super.visitMethodDeclaration(node);
    }
    visitGetAccessor(node) {
        if (this.checkShouldHaveVisibilityModifier(node)) {
            this.addFailureClassMemberVisibilityModifierMissing(node);
        }
        super.visitGetAccessor(node);
    }
    visitSetAccessor(node) {
        if (this.checkShouldHaveVisibilityModifier(node)) {
            this.addFailureClassMemberVisibilityModifierMissing(node);
        }
        super.visitSetAccessor(node);
    }
    nodeHasModifierToken(node, token) {
        return node.modifiers && node.modifiers.some(m => m.kind === token) || false;
    }
    checkShouldHaveVisibilityModifier(node) {
        let hasPublicModifier = this.nodeHasModifierToken(node, Ts.SyntaxKind.PublicKeyword);
        let hasPrivateModifier = this.nodeHasModifierToken(node, Ts.SyntaxKind.PrivateKeyword);
        let hasProtectedModifier = this.nodeHasModifierToken(node, Ts.SyntaxKind.ProtectedKeyword);
        return !hasPublicModifier && !hasPrivateModifier && !hasProtectedModifier;
    }
    addFailureClassMemberVisibilityModifierMissing(sourceNode) {
        let insertPoint;
        let memberType;
        // Insert the fix before any modifiers OR at the start of the element name token
        let firstModifier = sourceNode.modifiers && sourceNode.modifiers[0];
        if (firstModifier) {
            insertPoint = firstModifier.getStart();
        }
        else if (sourceNode.kind === Ts.SyntaxKind.GetAccessor || sourceNode.kind === Ts.SyntaxKind.SetAccessor) {
            // The AST does not appear to contain any information about the position of the get/set keywords.
            // Guess at their start position using (leading trivia + width of keyword)
            insertPoint = sourceNode.name.getStart() - (sourceNode.name.getLeadingTriviaWidth() + 3);
        }
        else {
            insertPoint = sourceNode.name.getStart();
        }
        memberType =
            sourceNode.kind === Ts.SyntaxKind.MethodDeclaration ? 'methods' :
                sourceNode.kind === Ts.SyntaxKind.GetAccessor ? 'getters' :
                    sourceNode.kind === Ts.SyntaxKind.SetAccessor ? 'setters' :
                        sourceNode.kind === Ts.SyntaxKind.PropertyDeclaration ? 'properties' : null;
        if (!memberType) {
            throw new Error(`Unexpected source node kind: ${Ts.SyntaxKind[sourceNode.kind]}`);
        }
        let fix = Lint.Replacement.appendText(insertPoint, 'public ');
        this.addFailureAt(sourceNode.name.getStart(), sourceNode.name.getWidth(), `Class ${memberType} must have a visibility modifier`, fix);
    }
}
