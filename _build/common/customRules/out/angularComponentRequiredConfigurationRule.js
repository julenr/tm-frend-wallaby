"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Lint = require("tslint");
const Ts = require("typescript");
const ANGULAR_CORE_MODULE = '@angular/core';
class Rule extends Lint.Rules.AbstractRule {
    apply(sourceFile) {
        // TODO: Configure from tslint.json arguments?
        let options = {
            shouldUseChangeDetection: 'OnPush',
            shouldUseViewEncapsulation: 'None'
        };
        return this.applyWithWalker(new AngularComponentsMustSpecifyWalker(sourceFile, 'angular-component-required-configuration', options));
    }
}
exports.Rule = Rule;
class AngularComponentsMustSpecifyWalker extends Lint.AbstractWalker {
    constructor() {
        super(...arguments);
        /** Map of Identifier/Alias -> Import info */
        this.importInfoMap = new Map();
    }
    walk(sourceFile) {
        let importsVisitor = (node) => {
            if (node.kind === Ts.SyntaxKind.ImportDeclaration) {
                this.scanImportDeclaration(node);
                return;
            }
            Ts.forEachChild(node, importsVisitor);
        };
        Ts.forEachChild(sourceFile, importsVisitor);
        let classDeclarationVisitor = (node) => {
            if (node.kind === Ts.SyntaxKind.ClassDeclaration) {
                this.verifyClassDeclarationHasCorrectAngularComponentConfiguration(node);
                return;
            }
            Ts.forEachChild(node, classDeclarationVisitor);
        };
        Ts.forEachChild(sourceFile, classDeclarationVisitor);
    }
    scanImportDeclaration(importDeclaration) {
        let moduleSpecifier = asStringLiteral(importDeclaration.moduleSpecifier);
        if (!moduleSpecifier) {
            throw new Error(`Invalid module specifier: ${importDeclaration.moduleSpecifier.getText()}`);
        }
        let importClause = importDeclaration.importClause;
        if (!importClause) {
            return;
        }
        // Hack hack - we're only really interested in ANGULAR_CORE imports...
        let sourceModule = moduleSpecifier.text;
        if (sourceModule !== ANGULAR_CORE_MODULE) {
            return;
        }
        // Support `import * as Ng from '@angular/core'`
        if (importClause.namedBindings.kind === Ts.SyntaxKind.NamespaceImport) {
            let namespaceImport = importClause.namedBindings;
            let alias = namespaceImport.name.text;
            this.importInfoMap.set(alias, { kind: 'namespace', alias, sourceModule });
            return;
        }
        // Support `import { Component } from '@angular/core'`
        let namedImports = importClause.namedBindings;
        for (let element of namedImports.elements || []) {
            let alias = element.name.text;
            let sourceIdentifier = (element.propertyName || element.name).text;
            this.importInfoMap.set(alias, { kind: 'named', alias, sourceIdentifier, sourceModule });
        }
    }
    /**
     * Looks up an identity path which might match an import and
     * rewrites the expression as module-relative for easy comparison.
     *
     *  E.g. 'Ng.Component.create' => { module: '@angular/core', canonicalPath: "Component.create" }
     *  E.g. 'Component.create'    => { module: '@angular/core', canonicalPath: "Component.create" }
     *  E.g. 'Foo.create'          => { module: '@angular/core', canonicalPath: "Component.create" }
     */
    tryResolveCanonicalIdentityPath(identityPath) {
        if (!identityPath) {
            return null;
        }
        let [alias, ...remainder] = identityPath.split('.');
        let importInfo = this.importInfoMap.get(alias);
        if (!importInfo) {
            return null;
        }
        // Namespace import?
        if (importInfo.kind === 'namespace') {
            return {
                module: importInfo.sourceModule,
                canonicalPath: remainder.join('.')
            };
        }
        // Named import (re-write alias)
        return {
            module: importInfo.sourceModule,
            canonicalPath: [importInfo.sourceIdentifier, ...remainder].join('.')
        };
    }
    verifyClassDeclarationHasCorrectAngularComponentConfiguration(classDeclaration) {
        for (let decorator of classDeclaration.decorators || []) {
            try {
                let config = this.tryParseComponentDecoratorConfiguration(decorator);
                if (!config) {
                    continue;
                }
                this.verifyComponentConfiguration(config);
            }
            catch (e) {
                this.addFailureAtNode(decorator, e.message);
            }
        }
    }
    tryParseComponentDecoratorConfiguration(decorator) {
        let constructorCall = asCallExpression(decorator.expression);
        if (!constructorCall) {
            return null;
        }
        // expression should be Identifier or PropertyAccessExpression
        if (constructorCall.expression.kind !== Ts.SyntaxKind.Identifier &&
            constructorCall.expression.kind !== Ts.SyntaxKind.PropertyAccessExpression) {
            return null;
        }
        // look up the source of the identifier..
        let identityLookup = this.tryResolveCanonicalIdentityPath(constructorCall.expression.getText());
        if (!identityLookup) {
            return null;
        }
        if (identityLookup.module !== ANGULAR_CORE_MODULE || identityLookup.canonicalPath !== 'Component') {
            // Not interested :/
            return null;
        }
        if (!constructorCall.arguments || constructorCall.arguments.length !== 1) {
            throw new Error('Unexpected @Component decorator arguments');
        }
        let literal = asObjectLiteralExpression(constructorCall.arguments[0]);
        if (!literal) {
            return null;
        }
        let tryFindPropertyInitializationExpression = (name) => {
            for (let prop of literal.properties || []) {
                let assignment = asPropertyAssignment(prop);
                if (assignment && tryGetPropertyAssignmentName(assignment.name) === name) {
                    return assignment.initializer;
                }
            }
            return null;
        };
        // select changeDetection and encapsualation
        return {
            root: decorator,
            templateUrl: tryFindPropertyInitializationExpression('templateUrl'),
            template: tryFindPropertyInitializationExpression('template'),
            styleUrls: tryFindPropertyInitializationExpression('styleUrls'),
            styles: tryFindPropertyInitializationExpression('styles'),
            changeDetection: tryFindPropertyInitializationExpression('changeDetection'),
            encapsulation: tryFindPropertyInitializationExpression('encapsulation')
        };
    }
    verifyComponentConfiguration(config) {
        // TODO: Enforce that templateUrl/styleUrls are relative paths only? PDN-1091
        // TODO: `encapsulation` only actually required if the config also specifies styles?
        if (this.options.shouldUseViewEncapsulation) {
            let encapsulationExpected = `ViewEncapsulation.${this.options.shouldUseViewEncapsulation}`;
            let encapsulationLookup = this.tryResolveCanonicalIdentityPath(config.encapsulation && config.encapsulation.getText());
            if (!encapsulationLookup || encapsulationLookup.module !== ANGULAR_CORE_MODULE || encapsulationLookup.canonicalPath !== encapsulationExpected) {
                this.addFailureAtNode(config.encapsulation || config.root, `Expected ${encapsulationExpected}`);
            }
        }
        if (this.options.shouldUseChangeDetection) {
            let changeDetectionExpected = `ChangeDetectionStrategy.${this.options.shouldUseChangeDetection}`;
            let changeDetectionLookup = this.tryResolveCanonicalIdentityPath(config.changeDetection && config.changeDetection.getText());
            if (!changeDetectionLookup || changeDetectionLookup.module !== ANGULAR_CORE_MODULE || changeDetectionLookup.canonicalPath !== changeDetectionExpected) {
                this.addFailureAtNode(config.changeDetection || config.root, `Expected ${changeDetectionExpected}`);
            }
        }
    }
}
function asObjectLiteralExpression(node) {
    return node && node.kind === Ts.SyntaxKind.ObjectLiteralExpression ? node : null;
}
function asCallExpression(node) {
    return node && node.kind === Ts.SyntaxKind.CallExpression ? node : null;
}
function asStringLiteral(node) {
    return node && node.kind === Ts.SyntaxKind.StringLiteral ? node : null;
}
function asPropertyAssignment(node) {
    return node && node.kind === Ts.SyntaxKind.PropertyAssignment ? node : null;
}
function tryGetPropertyAssignmentName(propertyName) {
    if (propertyName.kind === Ts.SyntaxKind.Identifier ||
        propertyName.kind === Ts.SyntaxKind.StringLiteral) {
        return propertyName.text;
    }
    return null;
}
function isArrayLiteralExpression(node) {
    return node && node.kind === Ts.SyntaxKind.ArrayLiteralExpression;
}
