Modifying custom lint rules:

1. Edit the rules in `~/_build/common/customRules/src`
2. Run `yarn lint` - this will automatically recompile the custom rules before running the linter.

All new rule files must be end with "Rule". E.g.

    ensureFoosAreNotFollowedByBarsRule.ts

To enable new rules in the project lint config, edit `~/tslint.json` and add a kebab-case
version of the lint name (excluding the "Rule" posfix):

    {
        "ensure-foos-are-not-followed-by-bars": true
    }

Instructions for writing new lint rules using TypeScript:

    https://palantir.github.io/tslint/develop/custom-rules/