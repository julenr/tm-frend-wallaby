import * as Lint from 'tslint';
import * as Ts from 'typescript';

const RULE_NAME = 'no-imports-from-module-blacklist';

interface IRuleConfiguration {
    bannedModulePatterns: RegExp[];
}

export class Rule extends Lint.Rules.AbstractRule {
    public apply (sourceFile: Ts.SourceFile): Lint.RuleFailure[] {
        // NOTE: Input arguments are a list of case-insensitive regular expressions
        let args = this.getOptions().ruleArguments;
        if (!args || !args.length) {
            throw new Error(`${RULE_NAME}: No module patterns defined in tslint.json!`);
        }
        let bannedModulePatterns = args.map(arg => new RegExp(arg, 'i'));
        return this.applyWithWalker(
            new NoDirectImportsFromWalker(sourceFile, RULE_NAME, { bannedModulePatterns })
        );
    }
}

class NoDirectImportsFromWalker extends Lint.AbstractWalker<IRuleConfiguration> {

    public walk (sourceFile: Ts.SourceFile) {

        let importsVisitor = (node: Ts.Node) => {
            if (node.kind === Ts.SyntaxKind.ImportDeclaration) {
                this.scanImportDeclaration(node as Ts.ImportDeclaration);
                return;
            }
            Ts.forEachChild(node, importsVisitor);
        };

        Ts.forEachChild(sourceFile, importsVisitor);
    }

    public scanImportDeclaration (importDeclaration: Ts.ImportDeclaration) {

        let moduleSpecifier = asStringLiteral(importDeclaration.moduleSpecifier);
        if (!moduleSpecifier) {
            throw new Error(`${RULE_NAME}: Invalid module specifier: ${importDeclaration.moduleSpecifier.getText()}`);
        }

        for (let pattern of this.options.bannedModulePatterns) {
            if (pattern.test(moduleSpecifier.text)) {
                let error = `Imports from '${moduleSpecifier.text}' are not allowed! Can you import from a more specific module?`;
                this.addFailureAtNode(moduleSpecifier, error);
            }
        }
    }
}

function asStringLiteral (node: Ts.Node): Ts.StringLiteral {
    return node && node.kind === Ts.SyntaxKind.StringLiteral ? node as Ts.StringLiteral : null;
}
