import * as Lint from 'tslint';
import * as Ts from 'typescript';

const ANGULAR_CORE_MODULE = '@angular/core';

export class Rule extends Lint.Rules.AbstractRule {
    public apply (sourceFile: Ts.SourceFile): Lint.RuleFailure[] {
        // TODO: Configure from tslint.json arguments?
        let options: IOptions = {
            shouldUseChangeDetection: 'OnPush',
            shouldUseViewEncapsulation: 'None'
        };
        return this.applyWithWalker(
            new AngularComponentsMustSpecifyWalker(sourceFile, 'angular-component-required-configuration', options)
        );
    }
}

interface IOptions {
    shouldUseChangeDetection?: 'Default'|'OnPush';
    shouldUseViewEncapsulation?: 'None'|'Emulated'|'Native';
}

type ImportInfo = INamedImport|INamespaceImport;
interface INamedImport {
    kind: 'named';
    alias: string;
    sourceIdentifier: string;
    sourceModule: string;
}
interface INamespaceImport {
    kind: 'namespace';
    alias: string;
    sourceModule: string;
}

interface IComponentConfig {
    root: Ts.Node;

    templateUrl: Ts.StringLiteral;
    template: Ts.StringLiteral;

    styleUrls: Ts.ArrayLiteralExpression;
    styles: Ts.ArrayLiteralExpression;

    changeDetection: Ts.Expression;
    encapsulation: Ts.Expression;
}

class AngularComponentsMustSpecifyWalker extends Lint.AbstractWalker<IOptions> {

    /** Map of Identifier/Alias -> Import info */
    public importInfoMap = new Map<string, ImportInfo>();

    public walk (sourceFile: Ts.SourceFile) {

        let importsVisitor = (node: Ts.Node) => {
            if (node.kind === Ts.SyntaxKind.ImportDeclaration) {
                this.scanImportDeclaration(node as Ts.ImportDeclaration);
                return;
            }
            Ts.forEachChild(node, importsVisitor);
        };

        Ts.forEachChild(sourceFile, importsVisitor);

        let classDeclarationVisitor = (node: Ts.Node) => {
            if (node.kind === Ts.SyntaxKind.ClassDeclaration) {
                this.verifyClassDeclarationHasCorrectAngularComponentConfiguration(node as Ts.ClassDeclaration);
                return;
            }
            Ts.forEachChild(node, classDeclarationVisitor);
        };

        Ts.forEachChild(sourceFile, classDeclarationVisitor);
    }

    private scanImportDeclaration (importDeclaration: Ts.ImportDeclaration) {
        let moduleSpecifier = asStringLiteral(importDeclaration.moduleSpecifier);
        if (!moduleSpecifier) {
            throw new Error(`Invalid module specifier: ${importDeclaration.moduleSpecifier.getText()}`);
        }
        let importClause = importDeclaration.importClause;
        if (!importClause) {
            return;
        }
        // Hack hack - we're only really interested in ANGULAR_CORE imports...
        let sourceModule = moduleSpecifier.text;
        if (sourceModule !== ANGULAR_CORE_MODULE) {
            return;
        }
        // Support `import * as Ng from '@angular/core'`
        if (importClause.namedBindings.kind === Ts.SyntaxKind.NamespaceImport) {
            let namespaceImport = importClause.namedBindings as Ts.NamespaceImport;
            let alias = namespaceImport.name.text;
            this.importInfoMap.set(alias, { kind: 'namespace', alias, sourceModule });
            return;
        }
        // Support `import { Component } from '@angular/core'`
        let namedImports = importClause.namedBindings as Ts.NamedImports;
        for (let element of namedImports.elements || []) {
            let alias = element.name.text;
            let sourceIdentifier = (element.propertyName || element.name).text;
            this.importInfoMap.set(alias, { kind: 'named', alias, sourceIdentifier, sourceModule });
        }
    }

    /**
     * Looks up an identity path which might match an import and
     * rewrites the expression as module-relative for easy comparison.
     *
     *  E.g. 'Ng.Component.create' => { module: '@angular/core', canonicalPath: "Component.create" }
     *  E.g. 'Component.create'    => { module: '@angular/core', canonicalPath: "Component.create" }
     *  E.g. 'Foo.create'          => { module: '@angular/core', canonicalPath: "Component.create" }
     */
    private tryResolveCanonicalIdentityPath (identityPath: string) {
        if (!identityPath) {
            return null;
        }
        let [alias, ...remainder] = identityPath.split('.');
        let importInfo = this.importInfoMap.get(alias);
        if (!importInfo) {
            return null;
        }
        // Namespace import?
        if (importInfo.kind === 'namespace') {
            return {
                module: importInfo.sourceModule,
                canonicalPath: remainder.join('.')
            };
        }
        // Named import (re-write alias)
        return {
            module: importInfo.sourceModule,
            canonicalPath: [ importInfo.sourceIdentifier, ...remainder ].join('.')
        };
    }

    private verifyClassDeclarationHasCorrectAngularComponentConfiguration (classDeclaration: Ts.ClassDeclaration) {

        for (let decorator of classDeclaration.decorators || []) {
            try {
                let config = this.tryParseComponentDecoratorConfiguration(decorator);
                if (!config) {
                    continue;
                }
                this.verifyComponentConfiguration(config);
            } catch (e) {
                this.addFailureAtNode(decorator, e.message);
            }
        }
    }

    private tryParseComponentDecoratorConfiguration (decorator: Ts.Decorator): IComponentConfig {
        let constructorCall = asCallExpression(decorator.expression);
        if (!constructorCall) {
            return null;
        }
        // expression should be Identifier or PropertyAccessExpression
        if (constructorCall.expression.kind !== Ts.SyntaxKind.Identifier &&
            constructorCall.expression.kind !== Ts.SyntaxKind.PropertyAccessExpression) {
            return null;
        }
        // look up the source of the identifier..
        let identityLookup = this.tryResolveCanonicalIdentityPath(constructorCall.expression.getText());
        if (!identityLookup) {
            return null;
        }
        if (identityLookup.module !== ANGULAR_CORE_MODULE || identityLookup.canonicalPath !== 'Component') {
            // Not interested :/
            return null;
        }
        if (!constructorCall.arguments || constructorCall.arguments.length !== 1) {
            throw new Error('Unexpected @Component decorator arguments');
        }
        let literal = asObjectLiteralExpression(constructorCall.arguments[0]);
        if (!literal) {
            return null;
        }
        let tryFindPropertyInitializationExpression = (name: string) => {
            for (let prop of literal.properties || []) {
                let assignment = asPropertyAssignment(prop);
                if (assignment && tryGetPropertyAssignmentName(assignment.name) === name) {
                    return assignment.initializer;
                }
            }
            return null;
        };
        // select changeDetection and encapsualation
        return {
            root: decorator,
            templateUrl:     tryFindPropertyInitializationExpression('templateUrl') as Ts.StringLiteral,
            template:        tryFindPropertyInitializationExpression('template') as Ts.StringLiteral,
            styleUrls:       tryFindPropertyInitializationExpression('styleUrls') as Ts.ArrayLiteralExpression,
            styles:          tryFindPropertyInitializationExpression('styles') as Ts.ArrayLiteralExpression,
            changeDetection: tryFindPropertyInitializationExpression('changeDetection'),
            encapsulation:   tryFindPropertyInitializationExpression('encapsulation')
        };
    }

    private verifyComponentConfiguration (config: IComponentConfig) {

        // TODO: Enforce that templateUrl/styleUrls are relative paths only? PDN-1091

        // TODO: `encapsulation` only actually required if the config also specifies styles?
        if (this.options.shouldUseViewEncapsulation) {
            let encapsulationExpected = `ViewEncapsulation.${this.options.shouldUseViewEncapsulation}`;
            let encapsulationLookup = this.tryResolveCanonicalIdentityPath(config.encapsulation && config.encapsulation.getText());
            if (!encapsulationLookup || encapsulationLookup.module !== ANGULAR_CORE_MODULE || encapsulationLookup.canonicalPath !== encapsulationExpected) {
                this.addFailureAtNode(config.encapsulation || config.root, `Expected ${encapsulationExpected}`);
            }
        }

        if (this.options.shouldUseChangeDetection) {
            let changeDetectionExpected = `ChangeDetectionStrategy.${this.options.shouldUseChangeDetection}`;
            let changeDetectionLookup = this.tryResolveCanonicalIdentityPath(config.changeDetection && config.changeDetection.getText());
            if (!changeDetectionLookup || changeDetectionLookup.module !== ANGULAR_CORE_MODULE || changeDetectionLookup.canonicalPath !== changeDetectionExpected) {
                this.addFailureAtNode(config.changeDetection || config.root, `Expected ${changeDetectionExpected}`);
            }
        }
    }
}

function asObjectLiteralExpression (node: Ts.Node): Ts.ObjectLiteralExpression {
    return node && node.kind === Ts.SyntaxKind.ObjectLiteralExpression ? node as Ts.ObjectLiteralExpression : null;
}

function asCallExpression (node: Ts.Node): Ts.CallExpression {
    return node && node.kind === Ts.SyntaxKind.CallExpression ? node as Ts.CallExpression : null;
}

function asStringLiteral (node: Ts.Node): Ts.StringLiteral {
    return node && node.kind === Ts.SyntaxKind.StringLiteral ? node as Ts.StringLiteral : null;
}

function asPropertyAssignment (node: Ts.Node): Ts.PropertyAssignment {
    return node && node.kind === Ts.SyntaxKind.PropertyAssignment ? node as Ts.PropertyAssignment : null;
}

function tryGetPropertyAssignmentName (propertyName: Ts.PropertyName): string {
    if (propertyName.kind === Ts.SyntaxKind.Identifier ||
        propertyName.kind === Ts.SyntaxKind.StringLiteral) {
        return propertyName.text;
    }
    return null;
}

function isArrayLiteralExpression (node: Ts.Node): node is Ts.ArrayLiteralExpression {
    return node && node.kind === Ts.SyntaxKind.ArrayLiteralExpression;
}