const ts = require('typescript');
const Lint = require('tslint');
const SUBSCRIBE_STRING = 'subscribe(';

function Rule () {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    let options = this.getOptions();
    let fileFilters = options.ruleArguments[0];
    if (fileFilters && fileFilters.every(fileFilter => syntaxTree.fileName.indexOf(fileFilter) === -1)) {
        // short-circuit on non spec files, for speed
        return [];
    }
    if (syntaxTree.text.indexOf(SUBSCRIBE_STRING) === -1) {
        // short-circuit when no subscriptions are present
        return [];
    }
    return this.applyWithWalker(new MustCallNgOnDestroyWhenSubscribing(syntaxTree, options));
};

function MustCallNgOnDestroyWhenSubscribing () {
    Lint.RuleWalker.apply(this, arguments);
}

MustCallNgOnDestroyWhenSubscribing.prototype = Object.create(Lint.RuleWalker.prototype);

MustCallNgOnDestroyWhenSubscribing.prototype.visitSourceFile = function (node) {

    let subscribeRegex = /^[^/\n\w]*subscribe/gm;
    let takeRegex = /^[^/\n\w]*\.take\(1\)/gm;
    let firstRegex = /^[^/\n\w]*\.first\(\)/gm;
    let takeUntilRegex = /^[^/\n\w]*\.takeUntil\(this\._destroy/gm;
    let unsubscribeRegex = /^[^/\n\w]*\.unsubscribe\(\)/gm;

    let subscriptionCount = (node.text.match(subscribeRegex) || []).length;
    let takeCount = (node.text.match(takeRegex) || []).length;
    let firstCount = (node.text.match(firstRegex) || []).length;
    let takeUntilCount = (node.text.match(takeUntilRegex) || []).length;
    let unsubscribeCount = (node.text.match(unsubscribeRegex) || []).length;

    // if there is a subscribe and no ngOnDestroy or .take(x) or takeUntil(...) we add error
    if (subscriptionCount > firstCount + takeCount + takeUntilCount + unsubscribeCount) {
        let position = node.text.indexOf(SUBSCRIBE_STRING);

        const failureMsg = `You must unsubscribe from all subscriptions in ngOnDestroy, or use take(1) or takeUntil(this._destroy) or .first()`;
        this.addFailure(this.createFailure(position, node.getWidth(), failureMsg));
    }
    Lint.RuleWalker.prototype.visitSourceFile.call(this, node);
};

exports.Rule = Rule;
