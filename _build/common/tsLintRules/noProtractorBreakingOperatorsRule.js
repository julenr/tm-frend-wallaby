const Lint = require('tslint');

const RXJS_FAILURE_MSG = `Please do not use rxjs "delay" or "interval" operators, as they break protractor. Instead, inject DelayService and use the operators on that.`;
const TIMEOUT_INTERVAL_FAILURE_MSG = `Please use delayService for native timeouts and intervals.`;

const TIMEOUT_INTERVAL_CALL_REG_EX = /set(Interval|Timeout)\(/gm;

const BANNED_OPERATORS = [
    'rxjs/add/operator/delay',
    'rxjs/add/observable/interval'
];

function Rule () {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    return this.applyWithWalker(new NoProtractorBreakingOperators(syntaxTree, this.getOptions()));
};

function NoProtractorBreakingOperators () {
    Lint.RuleWalker.apply(this, arguments);
}

NoProtractorBreakingOperators.prototype = Object.create(Lint.RuleWalker.prototype);

NoProtractorBreakingOperators.prototype.visitImportDeclaration = function (node) {
    const importPath = node.moduleSpecifier.text;
    const containsBannedOperator = BANNED_OPERATORS.some(o => o === importPath);
    if (containsBannedOperator) {
        this.addFailure(this.createFailure(node.getStart(), node.getWidth(), RXJS_FAILURE_MSG));
    }
    Lint.RuleWalker.prototype.visitImportDeclaration.call(this, node);
};

NoProtractorBreakingOperators.prototype.visitSourceFile = function (node) {
    let match;

    while (match = TIMEOUT_INTERVAL_CALL_REG_EX.exec(node.text)) {
        this.addFailure(this.createFailure(match.index, match[0].length, TIMEOUT_INTERVAL_FAILURE_MSG));
    }

    Lint.RuleWalker.prototype.visitSourceFile.call(this, node);
};

exports.Rule = Rule;
