var Lint = require('tslint');

function Rule() {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    let options = this.getOptions();
    let fileFilter = options.ruleArguments[0];
    if (fileFilter && syntaxTree.fileName.indexOf(fileFilter) === -1) {
        // short-circuit on non spec files, for speed
        return [];
    }
    return this.applyWithWalker(new ProviderIneedaFactoryWalker(syntaxTree, options));
};
function ProviderIneedaFactoryWalker() {
    Lint.RuleWalker.apply(this, arguments);
}

ProviderIneedaFactoryWalker.prototype = Object.create(Lint.RuleWalker.prototype);

ProviderIneedaFactoryWalker.prototype.visitObjectLiteralExpression = function (node) {
    if (node._children.length) {
        node.forEachChild(child => {
            if (child.name && child.name.text === 'providers') {
                // found providers object
                child.forEachChild(providersChild => {
                    if (providersChild.elements) {
                        // found array of provider objects
                        providersChild.elements.forEach(provider => {
                            if (provider.constructor.name === 'NodeObject') {
                                provider.forEachChild(providerChild => {
                                    // found provider with useClass 
                                    if (providerChild.name && providerChild.name.text === 'useClass') {
                                        if (providerChild.getChildCount() && providerChild.getChildAt(providerChild.getChildCount() - 1).expression) {
                                            let useClassValue = providerChild.getChildAt(providerChild.getChildCount() - 1).expression.getFullText().trim();
                                            if (useClassValue === 'ineeda') {
                                                let useClassNode = providerChild.getChildAt(providerChild.getChildCount() - 1);
                                                this.addFailure(this.createFailure(useClassNode.getStart(), useClassNode.getWidth(), "Providers defined with 'useClass' must use 'ineeda.factory' rather than just 'ineeda'."));
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    Lint.RuleWalker.prototype.visitObjectLiteralExpression(node);
};
exports.Rule = Rule;
