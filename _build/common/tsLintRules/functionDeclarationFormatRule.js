var ts = require('typescript');
var Lint = require('tslint');

function Rule() {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    return this.applyWithWalker(new FunctionDeclarationFormatWalker(syntaxTree, this.getOptions()));
};

function FunctionDeclarationFormatWalker() {
    Lint.RuleWalker.apply(this, arguments);
}

FunctionDeclarationFormatWalker.prototype = Object.create(Lint.RuleWalker.prototype);

FunctionDeclarationFormatWalker.prototype.visitTokens = function (node, tokenOrder) {
    var scanner = ts.createScanner(ts.ScriptTarget.ES5, false, ts.LanguageVariant.Standard, node.getText());
    var textPos = 0;
    var prevTextPos = 0;

    while (scanner.scan() && tokenOrder.length) {
        prevTextPos = textPos;
        textPos = scanner.getTextPos();
        let toke = scanner.getToken();

        var expectedNextToken = tokenOrder[0];
        if (expectedNextToken.tokens && expectedNextToken.tokens.indexOf(toke) >= 0) {
            tokenOrder.splice(0, 1);
            continue;
        } else if (expectedNextToken.token && toke === expectedNextToken.token) {
            tokenOrder.splice(0, 1);
            continue;
        } else if (expectedNextToken.optional) {
            textPos = prevTextPos;
            scanner.setTextPos(prevTextPos);
            tokenOrder.splice(0, 1);
            continue;
        } else {
            this.addFailure(this.createFailure(node.getStart(), node.getWidth(), expectedNextToken.message));
            return;
        }
    }
};

FunctionDeclarationFormatWalker.prototype.visitGetAccessor = function (node) {
    var tokenOrder = [
        {token: ts.SyntaxKind.AtToken, optional: true},
        {token: ts.SyntaxKind.Identifier, optional: true },
        {token: ts.SyntaxKind.OpenParenToken, optional: true },
        {token: ts.SyntaxKind.StringLiteral, optional: true },
        {token: ts.SyntaxKind.Identifier, optional: true },
        {token: ts.SyntaxKind.CloseParenToken, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.NewLineTrivia, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {tokens: [
            ts.SyntaxKind.PublicKeyword,
            ts.SyntaxKind.ProtectedKeyword,
            ts.SyntaxKind.PrivateKeyword
        ], optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.StaticKeyword, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.GetKeyword, message: "Get Accessor doesn't have Get keyword" },
        {token: ts.SyntaxKind.WhitespaceTrivia, message: "Get Accessor doesn't have whitespace after Get keyword" },
        {tokens: [
            ts.SyntaxKind.Identifier,
            ts.SyntaxKind.IsKeyword
        ], optional: false, message: "Get Accessor doesn't have an identifier" },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Get Accessor doesn't have a whitespace after indicator and before open parenthesis" },
        {token: ts.SyntaxKind.OpenParenToken, optional: false, message: "Get Accessor doesn't have an open parenthesis" },
    ];

    this.visitTokens(node, tokenOrder);
    Lint.RuleWalker.prototype.visitGetAccessor.call(this, node);
    return;
};

FunctionDeclarationFormatWalker.prototype.visitSetAccessor = function (node) {
    var tokenOrder = [
        {token: ts.SyntaxKind.AtToken, optional: true},
        {token: ts.SyntaxKind.Identifier, optional: true },
        {token: ts.SyntaxKind.OpenParenToken, optional: true },
        {token: ts.SyntaxKind.StringLiteral, optional: true },
        {token: ts.SyntaxKind.Identifier, optional: true },
        {token: ts.SyntaxKind.CloseParenToken, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.NewLineTrivia, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {tokens: [
            ts.SyntaxKind.PublicKeyword,
            ts.SyntaxKind.ProtectedKeyword,
            ts.SyntaxKind.PrivateKeyword
        ], optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.StaticKeyword, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.SetKeyword, message: "Set Accessor doesn't have Set keyword" },
        {token: ts.SyntaxKind.WhitespaceTrivia, message: "Set Accessor doesn't have whitespace after Set keyword" },
        {tokens: [
            ts.SyntaxKind.Identifier,
            ts.SyntaxKind.IsKeyword
        ], optional: false, message: "Set Accessor doesn't have an identifier" },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Set Accessor doesn't have a whitespace after indicator and before open parenthesis" },
        {token: ts.SyntaxKind.OpenParenToken, optional: false, message: "Set Accessor doesn't have an open parenthesis" },
    ];

    this.visitTokens(node, tokenOrder);

    Lint.RuleWalker.prototype.visitGetAccessor.call(this, node);
    return;
};

FunctionDeclarationFormatWalker.prototype.visitMethodSignature = function (node) {
    var tokenOrder = [
        {tokens: [
            ts.SyntaxKind.Identifier,
            ts.SyntaxKind.IsKeyword
        ], optional: false, message: "Method Signature doesn't have an identifier" },
        {token: ts.SyntaxKind.QuestionToken, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Method Signature doesn't have a whitespace after indicator and before open parenthesis" },
        {token: ts.SyntaxKind.OpenParenToken, optional: false, message: "Method Signature doesn't have an open parenthesis" },
    ];

    this.visitTokens(node, tokenOrder);
    Lint.RuleWalker.prototype.visitMethodSignature.call(this, node);
    return;
};

FunctionDeclarationFormatWalker.prototype.visitMethodDeclaration = function (node) {

    var tokenOrder = [
        {tokens: [
            ts.SyntaxKind.PublicKeyword,
            ts.SyntaxKind.ProtectedKeyword,
            ts.SyntaxKind.PrivateKeyword
        ], optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {token: ts.SyntaxKind.StaticKeyword, optional: true },
        {token: ts.SyntaxKind.AbstractKeyword, optional: true },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true },
        {tokens: [
            ts.SyntaxKind.Identifier,
            ts.SyntaxKind.IsKeyword
        ], optional: false, message: "Method Declaration doesn't have an identifier" },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Method Declaration doesn't have a whitespace after indicator and before open parenthesis" },
        {token: ts.SyntaxKind.OpenParenToken, optional: false, message: "Method Declaration doesn't have an open parenthesis" },
    ];

    this.visitTokens(node, tokenOrder);
    Lint.RuleWalker.prototype.visitMethodDeclaration.call(this, node);
    return;
};

FunctionDeclarationFormatWalker.prototype.visitFunctionDeclaration = function (node) {
    var tokenOrder = [
        {token: ts.SyntaxKind.ExportKeyword, optional: true},
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: true},
        {token: ts.SyntaxKind.FunctionKeyword, optional: false, message: "Function Declaration doesn't start with function" },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Function Declaration doesn't have a whitespace after function" },
        {tokens: [
            ts.SyntaxKind.Identifier,
            ts.SyntaxKind.IsKeyword
        ], optional: false, message: "Function Declaration doesn't have an identifier" },
        {token: ts.SyntaxKind.WhitespaceTrivia, optional: false, message: "Function Declaration doesn't have a whitespace after indicator and before open parenthesis" },
        {token: ts.SyntaxKind.OpenParenToken, optional: false, message: "Function Declaration doesn't have an open parenthesis" },
    ];

    this.visitTokens(node, tokenOrder);
    Lint.RuleWalker.prototype.visitFunctionDeclaration.call(this, node);
    return;
};

exports.Rule = Rule;
