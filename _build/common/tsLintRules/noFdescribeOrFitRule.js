const ts = require('typescript');
const Lint = require('tslint');

function Rule () {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    let options = this.getOptions();
    let fileFilter = options.ruleArguments[0];
    if (fileFilter && syntaxTree.fileName.indexOf(fileFilter) === -1) {
        // short-circuit on non spec files, for speed
        return [];
    }
    return this.applyWithWalker(new NoFdescribeOrFit(syntaxTree, options));
};

function NoFdescribeOrFit () {
    Lint.RuleWalker.apply(this, arguments);
}

NoFdescribeOrFit.prototype = Object.create(Lint.RuleWalker.prototype);

NoFdescribeOrFit.prototype.visitNode = function (node) {
    let isFdescribeOrFit = node.kind === ts.SyntaxKind.Identifier
        && (node.text === 'fdescribe' || node.text === 'fit');

    if (isFdescribeOrFit) {
        const failureMsg = `Remember to remove ${node.text} once you have finished working on tests.`;
        this.addFailure(this.createFailure(node.getStart(), node.getWidth(), failureMsg));
    }

    Lint.RuleWalker.prototype.visitNode.call(this, node);
};

exports.Rule = Rule;
