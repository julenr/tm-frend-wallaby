var Lint = require('tslint');

let failureMsg = "Don't import from the root 'rxjs' package, because it pulls in the entire RXJS library. Instead, import from the submodule, e.g. 'rxjs/Observable'.";

function Rule() {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    return this.applyWithWalker(new NoRxjsRootImport(syntaxTree, this.getOptions()));
};

function NoRxjsRootImport() {
    Lint.RuleWalker.apply(this, arguments);
}

NoRxjsRootImport.prototype = Object.create(Lint.RuleWalker.prototype);

NoRxjsRootImport.prototype.visitImportDeclaration = function (node) {
    if (node.moduleSpecifier.text === 'rxjs' || node.moduleSpecifier.text === 'rxjs/Rx') {
        this.addFailure(this.createFailure(node.getStart(), node.getWidth(), failureMsg));
    }
    Lint.RuleWalker.prototype.visitImportDeclaration.call(this, node);
};

exports.Rule = Rule;
