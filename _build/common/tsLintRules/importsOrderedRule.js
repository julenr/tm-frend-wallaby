/*
 "ordered-imports": [true, {

 }],
 https://palantir.github.io/tslint/rules/ordered-imports/
 */

const ts = require('typescript');
const Lint = require('tslint');

const FAILURE_MESSAGE = 'Import statements should be ordered alphabetically by their file path, with third party imports before TM imports.';

function Rule() {
    Lint.Rules.AbstractRule.apply(this, arguments);
}

Rule.prototype = Object.create(Lint.Rules.AbstractRule.prototype);
Rule.prototype.apply = function (syntaxTree) {
    return this.applyWithWalker(new ImportsOrderWalker(syntaxTree, this.getOptions()));
};
function ImportsOrderWalker() {
    Lint.RuleWalker.apply(this, arguments);
}

ImportsOrderWalker.prototype = Object.create(Lint.RuleWalker.prototype);

ImportsOrderWalker.prototype.visitSourceFile = function (node) {
    let fileContentArray = node.text.split(new RegExp('^\r\n?|\n'));

    let originalImports = [];
    let thirdPartyImports = [];
    let tmImports = [];

    let importsFileLength = 0;
    let currentMultiLineImport;
    let i;
    for (i = 0; i < fileContentArray.length; i++) {
        let row = fileContentArray[i];

        if (currentMultiLineImport) {
            currentMultiLineImport.push(row);

            if (isClosingMultiLineImport(row)) {
                originalImports.push(currentMultiLineImport);
                if (isTmImport(row)) {
                    tmImports.push(currentMultiLineImport);
                } else {
                    thirdPartyImports.push(currentMultiLineImport);
                }
                currentMultiLineImport = [];
            }

            importsFileLength += row.length;
        } else {
            if (isOpeningMultiLineImport(row)) {
                currentMultiLineImport = [row];
            } else if (isImportStatement(row)) {
                originalImports.push(row);
                if (isTmImport(row)) {
                    tmImports.push(row);
                } else {
                    thirdPartyImports.push(row);
                }
            }
            importsFileLength += row.length;
        }
    }

    let sorted3rdPartyImports = sortImportsByPath(thirdPartyImports);
    let sortedTmImports = sortImportsByPath(tmImports);
    let sortedImports = sorted3rdPartyImports.concat(sortedTmImports);

    let j;
    for (j = 0; j < originalImports.length; j++) {
        let orig = normaliseImport(originalImports[j]);
        let sorted = normaliseImport(sortedImports[j]);

        if (orig !== sorted) {

            let lastImport = normaliseImport(originalImports[originalImports.length - 1]);
            let foo = node.text.indexOf(lastImport) + lastImport.length;

            let fixedImports = getFixedImports(sorted3rdPartyImports, sortedTmImports);
             // console.log(foo);
            let fix = new Lint.Replacement(node.getStart(), foo, fixedImports);

            break;
        }
    }

    Lint.RuleWalker.prototype.visitSourceFile.call(this, node);
};

function normaliseImport (iport) {
    if (Array.isArray(iport)) {
        return iport[iport.length - 1];
    }
    return iport;
}

function isImportStatement (statement) {
    return statement.indexOf('import ') === 0;
}

function isOpeningMultiLineImport (statement) {
    return statement.indexOf('import {') === 0
        && statement.indexOf('}') === -1;
}

function isClosingMultiLineImport (statement) {
    return statement.indexOf (`from '`) >= 0;
}

function isTmImport (statement) {
    return statement.indexOf(`from '.`) >= 0;
}

function getPathFromImport(iport) {
    iport = normaliseImport(iport);
    let rexp = /from '(.+)';/;
    let match = iport.match(rexp);
    return match && match[1];
}

function sortImportsByPath (imports) {
    return imports.sort((a, b) => {
        let pathA = getPathFromImport(a);
        let pathB = getPathFromImport(b);

        return (pathA || '').localeCompare(pathB);
    });
}

function getFixedImports (sorted3rdPartyImports, sortedTmImports) {
    let fixed = sorted3rdPartyImports.map((imp) => {
        if (Array.isArray(imp)) {
            return imp.join('\r\n');
        }
        return imp;
    }).join('\r\n');

    if (fixed.length && sortedTmImports.length) {
        fixed += '\r\n\r\n';
    }

    fixed += sortedTmImports.join('\r\n');

    return fixed;
}

exports.Rule = Rule;
