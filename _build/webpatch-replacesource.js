const fs = require('fs-extra');
const path = require('path');
const { root } = require('../helpers');

const modulePath = './node_modules/webpack-sources';
const packageJsonPath = path.join(root(modulePath), 'package.json');

const PACKAGE_NAME = 'webpack-sources';
const PACKAGE_VERSION = '1.0.1';

console.log(`Patching ${PACKAGE_NAME}@${PACKAGE_VERSION} with fast ReplaceSource`);

if (!fs.existsSync(packageJsonPath)) {
    console.log(PACKAGE_NAME + ' package.json not found');
    return;
}

const packageJson = JSON.parse(fs.readFileSync(packageJsonPath));

if (packageJson.name !== PACKAGE_NAME) {
    console.log(`Package name does not match`);
    return;
}

if (packageJson.version !== PACKAGE_VERSION) {
    console.log(`Package version does not match`);
    return;
}

const srcPath = './_build/ReplaceSource.js';
const destPath = path.join(modulePath, './lib/ReplaceSource.js');

if (!fs.existsSync(srcPath)) {
    console.log('ReplaceSource.js file not found');
    return;
}

console.log(`  copying ${srcPath} to ${destPath}`);
fs.copySync(root(srcPath), root(destPath));

console.log('  done');