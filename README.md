# FrEnd

## Basic scripts

Use `npm start` for dev server.

Use `npm run start:hmr` to run dev server in HMR mode.

Use `npm run build` for production build.

Use `npm run server:prod` for production server and production watch. Default production port is `8088`.

Use `npm run universal` to run production build in Universal. To run and build universal in AOT mode, use
`npm run universal:aot`. Default universal port is `8000`.

To create AOT version, run `npm run compile`. This will compile and build script.
Then you can use `npm run prodserver` to see to serve files.

The scripts are set to compile css next to scss because ngc compiler does not support Sass.
To compile scss, use `npm run sass`, but many of the scripts will either build or watch scss files.


## Build Features

* Angular 2
  * Async loading
  * Treeshaking
  * AOT (Ahead of Time/ Offline) Compilation
  * AOT safe SASS compilation
* Webpack 2
  * Webpack Dlls (Speeds up devServer builds)
* HMR (Hot Module Replacement)
* TypeScript 2
  * @types
* Universal (Server-side Rendering)
* Karma/Jasmine testing
* Protractor for E2E testing

### HMR (Hot Module Replacement)

HMR mode allows you to update a particular module without reloading the entire application.

### AOT  Don'ts

The following are some things that will make AOT compile fail.

- Don’t use require statements for your templates or styles, use styleUrls and templateUrls, the angular2-template-loader plugin will change it to require at build time.
- Don’t use default exports.
- Don’t use form.controls.controlName, use form.get(‘controlName’)
- Don’t use control.errors?.someError, use control.hasError(‘someError’)
- Don’t use functions in your providers, routes or declarations, export a function and then reference that function name
- Inputs, Outputs, View or Content Child(ren), Hostbindings, and any field you use from the template or annotate for Angular should be public

### Testing

For unit tests, use `npm run test` for continuous testing in watch mode and use
`npm run test:once` for single test. To view code coverage after running test, open `coverage/html/index.html` in your browser.

For e2e tests, use `npm run e2e`. To run unit test and e2e test at the same time, use `npm run ci`.
