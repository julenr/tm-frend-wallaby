var wallabyWebpack = require('wallaby-webpack');
var path = require('path');
var root = require('./helpers.js').root;

module.exports = function (wallaby) {

    var webpackPostprocessor = wallabyWebpack({
        entryPatterns: [
            'test-config/wallaby.test-entry.js',
            'src/app/**/*.spec.js'
        ],

        module: {
            loaders: [
                {
                    test: /\.ts$/,
                    include: /node_modules/,
                    exclude: [/\.(e2e|d)\.ts$/, root('node_modules/@angular'), root('node_modules/rxjs')],
                    use: [
                        {
                            loader: 'awesome-typescript-loader',
                            query: {
                                transpileOnly: true,
                                sourceMap: false,
                                inlineSourceMap: true,
                                compilerOptions: {
                                    removeComments: true
                                }
                            }
                        },
                        'angular2-template-loader'
                    ]
                },
                {test: /\.js$/, loader: 'angular2-template-loader', exclude: /node_modules/},
                {test: /\.(scss|css)$/, loaders: ['to-string-loader', 'null-loader']},
                {test: /\.html$/, loader: 'raw-loader'},
                {test: /\.svg/, loaders: ['to-string-loader', 'null-loader']}
            ]
        },

        resolve: {
            extensions: ['.js', '.ts'],
            alias: {
                '@trademe/core': path.join(wallaby.projectCacheDir, 'src/app/core'),
                '@trademe/testing': path.join(wallaby.projectCacheDir, 'test-config')
            },
            modules: [
                path.join(wallaby.projectCacheDir),
                'node_modules'
            ]
        }
    });

    return {
        files: [
            {pattern: 'test-config/wallaby.test-entry.js', load: false},
            {pattern: 'test-config/*.ts', load: false},
            {pattern: 'src/**/*.+(ts|css|scss|html|json|svg)', load: false},
            {pattern: 'src/**/*.d.ts', ignore: true},
            {pattern: 'node_modules/@angular/router/src/config.d.ts', instrument: false, load: false},
            {pattern: 'src/**/*.spec.ts', ignore: true}
        ],

        tests: [
            {pattern: 'src/app/**/*spec.ts', load: false}
        ],

        testFramework: 'jasmine',

        middleware: function (app, express) {
            var path = require('path');
            app.use('/favicon.ico', express.static(path.join(__dirname, 'src/favicon.ico')));
            app.use('/assets', express.static(path.join(__dirname, 'src/assets')));
        },

        env: {
            kind: 'chrome'
        },

        postprocessor: webpackPostprocessor,

        setup: function () {
            window.__moduleBundler.loadTests();
        },

        debug: true
    };
};
