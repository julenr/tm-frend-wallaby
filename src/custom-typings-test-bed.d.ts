import { Component, Directive, Injector, NgModule, Pipe, PlatformRef, Type } from '@angular/core';
import { ComponentFixture, MetadataOverride, TestModuleMetadata } from '@angular/core/testing';

type Klass<T> = {
    new (...args: any[]): T
};

declare module '@angular/core/testing' {

    /**
     *                           _,..__,
     *                       ,.'''      `"-,_
     *                     ,'                '.
     *                   ,'                    '
     *                  /                       \_
     *                 ;     -.                   `\
     *                 |       |     _         _    .
     *                ;       ,'  ,-' `.     /' `.  |
     *                |       '  /  o   |   t  o  \.'    .,-.
     *                 |         |:    .'   |:    .|    /    \
     *                 ;         \:.._./    ':_..:/ `. |      L
     *                  \  ,-'           |\_         `\-     "'-.
     *      ,-"'``'-,    `f              '/`>                    `.
     *    ,'        `L___.|              '  `     . _,/            \
     *    |                \_          _   _    .-.]____,,r        |
     *    \             ,. ___""----./` \,' ',`\'  \      \     mx.'
     *      `'-'|        '`         `|   |   |  |  |       `'--"'`
     *          ,         |           L_.'.__:__.-'
     *           \        /
     *            `'-- "'`
     *
     * DANGER, WILL ROBINSON:
     *  This class declaration is a copy/paste of the `TestBed` class from `@angular/core/testing` with
     *  some modifications to improve the typings.
     *
     * Be careful to keep it up to date with any changes to the original class declaration.
     */

    /**
     * @whatItDoes Configures and initializes environment for unit testing and provides methods for
     * creating components and services in unit tests.
     * @description
     *
     * TestBed is the primary api for writing unit tests for Angular applications and libraries.
     *
     * @stable
     */
    class TestBed implements Injector {
        // TRADEME OVERRIDES
        public static get<T>      (token: Klass<T>, notFoundValue?: T): T;
        public static get<T = {}> (token: any,      notFoundValue?: T): T;

        public get<T>      (token: Klass<T>, notFoundValue?: T): T;
        public get<T = {}> (token: any,      notFoundValue?: T): T;

        /**
         * Initialize the environment for testing with a compiler factory, a PlatformRef, and an
         * angular module. These are common to every test in the suite.
         *
         * This may only be called once, to set up the common providers for the current test
         * suite on the current platform. If you absolutely need to change the providers,
         * first use `resetTestEnvironment`.
         *
         * Test modules and platforms for individual platforms are available from
         * '@angular/<platform_name>/testing'.
         *
         * @experimental
         */
        public static initTestEnvironment (ngModule: Type<any> | Type<any>[], platform: PlatformRef): TestBed;
        /**
         * Reset the providers for the test injector.
         *
         * @experimental
         */
        public static resetTestEnvironment (): void;
        public static resetTestingModule (): typeof TestBed;
        /**
         * Allows overriding default compiler providers and settings
         * which are defined in test_injector.js
         */
        public static configureCompiler (config: {
            providers?: any[];
            useJit?: boolean;
        }): typeof TestBed;
        /**
         * Allows overriding default providers, directives, pipes, modules of the test injector,
         * which are defined in test_injector.js
         */
        public static configureTestingModule (moduleDef: TestModuleMetadata): typeof TestBed;
        /**
         * Compile components with a `templateUrl` for the test's NgModule.
         * It is necessary to call this function
         * as fetching urls is asynchronous.
         */
        public static compileComponents (): Promise<any>;
        public static overrideModule (ngModule: Type<any>, override: MetadataOverride<NgModule>): typeof TestBed;
        public static overrideComponent (component: Type<any>, override: MetadataOverride<Component>): typeof TestBed;
        public static overrideDirective (directive: Type<any>, override: MetadataOverride<Directive>): typeof TestBed;
        public static overridePipe (pipe: Type<any>, override: MetadataOverride<Pipe>): typeof TestBed;
        public static overrideTemplate (component: Type<any>, template: string): typeof TestBed;
        public static createComponent<T> (component: Type<T>): ComponentFixture<T>;
        private _instantiated;
        private _compiler;
        private _moduleRef;
        private _moduleWithComponentFactories;
        private _compilerOptions;
        private _moduleOverrides;
        private _componentOverrides;
        private _directiveOverrides;
        private _pipeOverrides;
        private _providers;
        private _declarations;
        private _imports;
        private _schemas;
        private _activeFixtures;
        /**
         * Initialize the environment for testing with a compiler factory, a PlatformRef, and an
         * angular module. These are common to every test in the suite.
         *
         * This may only be called once, to set up the common providers for the current test
         * suite on the current platform. If you absolutely need to change the providers,
         * first use `resetTestEnvironment`.
         *
         * Test modules and platforms for individual platforms are available from
         * '@angular/<platform_name>/testing'.
         *
         * @experimental
         */
        public initTestEnvironment (ngModule: Type<any> | Type<any>[], platform: PlatformRef): void;
        /**
         * Reset the providers for the test injector.
         *
         * @experimental
         */
        public resetTestEnvironment (): void;
        public resetTestingModule (): void;
        public platform: PlatformRef;
        public ngModule: Type<any> | Type<any>[];
        public configureCompiler (config: {
            providers?: any[];
            useJit?: boolean;
        }): void;
        public configureTestingModule (moduleDef: TestModuleMetadata): void;
        public compileComponents (): Promise<any>;
        private _initIfNeeded ();
        private _createCompilerAndModule ();
        private _assertNotInstantiated (methodName, methodDescription);
        public execute (tokens: any[], fn: Function, context?: any): any;
        public overrideModule (ngModule: Type<any>, override: MetadataOverride<NgModule>): void;
        public overrideComponent (component: Type<any>, override: MetadataOverride<Component>): void;
        public overrideDirective (directive: Type<any>, override: MetadataOverride<Directive>): void;
        public overridePipe (pipe: Type<any>, override: MetadataOverride<Pipe>): void;
        public createComponent<T> (component: Type<T>): ComponentFixture<T>;
    }
}