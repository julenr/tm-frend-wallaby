/* COMPANY */
Name: Trade Me
URL: trademe.co.nz
Twitter: @TradeMe
Location: Wellington, New Zealand

/* TEAM */
Simon Young - Chief Nerd - @tarkwyn
Elwyn Benson - Dev Tech Lead - elwyn@L1.co.nz
Matthew Brett - Senior Dev - @matthewbrett
Andre Sander - Harmonica Wizard - @_andresander
<name> - <role> - <twitter or other contact>

/* THANKS */
Thoughtram: thoughtram.io
<Name>: <name or url>

/* SITE */
Last update: 2017/07/06
Frameworks: Angular
Standards: HTML5, Sass, CSS3, TypeScript, ES5, ES6
IDE: WebStorm, VS Code, Sketch
DevOps: Node, WebPack, TeamCity, Mercurial


 _____              _        __  __
|_   _| __ __ _  __| | ___  |  \/  | ___    __ _
  | || '__/ _` |/ _` |/ _ \ | |\/| |/ _ \  /  ('>--
  | || | | (_| | (_| |  __/ | |  | |  __/  \__/
  |_||_|  \__,_|\__,_|\___| |_|  |_|\___|   L\_
  L i f e     l i v e s     h e r e
