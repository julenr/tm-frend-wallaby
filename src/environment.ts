import { enableDebugTools, disableDebugTools } from '@angular/platform-browser';
import { enableProdMode, ApplicationRef } from '@angular/core';

// Angular debug tools in the dev console
// https://github.com/angular/angular/blob/86405345b781a9dc2438c0fbe3e9409245647019/TOOLS_JS.md
let _decorateModuleRef = function identity<T> (value: T): T { return value; };

const ENABLE_PROD_MODE = ENV === 'production';
const EXPOSE_GLOBALS = !ENABLE_PROD_MODE;
const ENABLE_DEBUG_TOOLS = !ENABLE_PROD_MODE;

if (ENABLE_PROD_MODE) {
    // Production
    // TODO NG2 - commented it out to fix "Cannot read property 'setGlobalVar' of null" issue
    // https://github.com/qdouble/angular-webpack2-starter/issues/263
    // disableDebugTools();
    enableProdMode();
}

if (EXPOSE_GLOBALS) {
    _decorateModuleRef = (modRef: any) => {
        const appRef = modRef.injector.get(ApplicationRef);
        const cmpRef = appRef.components[0];

        let _ng = (<any>window).ng;
        if (ENABLE_DEBUG_TOOLS) {
            enableDebugTools(cmpRef);
        }
        (<any>window).ng.probe = _ng.probe;
        (<any>window).ng.coreTokens = _ng.coreTokens;
        return modRef;
    };
}

export const decorateModuleRef = _decorateModuleRef;

export const onAppFailedToBoot = function (error): void {
    /* tslint:disable */
    console.error(error);
    /* tslint:enable */
};
