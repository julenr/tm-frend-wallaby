(function () {
    /*  HERE BE DRAGONS!
     Be careful when editing this file - it can have serious consequences on the apps startup performance!

     This file is inlined into index.html at build time.
     This file has no transpile / polyfilling applied - ES5 only please.
     There are a few reasons we want an inline script doing stuff before angular has bootstrapped.

     1) During the Preview rollout, we want to redirect people away from Preview if they are not meant to be here, e.g.
     they're not in the right A/B group. We have a crappy div over the top of the app so they don't see a flash of Preview
     before they're redirected off to Touch. We delete that div in here if they've ever seen Preview before.
     2) We want some global error handling/logging so we can report errors back to the API, even if our app fails to start.
     3) There is a bunch of crappy TOuch-to-Preview auth token handover code. It is messy, hacky, and has confusing logic with
     tons of weird edge cases. I'm sorry, please avert your eyes. It will all go away soon(tm) when Touch is a) turned off
     or b) we set the AB framework to allow everyone access.
     */
    window.trademe = window.trademe || {};

    // TODO render env from Universal - PDN-368
    var env = (/([a-z\-0-9]+)\.trademe\.co\.nz/.exec(window.location.href)[1] || 'production')
        .replace('ng3-preview', 'ng3-production')
        .replace('preview', 'production');
    var isDev = env === 'dev';

    var ravenScriptSrc = window.__raven_js_src;

    // these two should happen as early as possible, so any bugs / crashes caused in the rest of the code are caught early.
    readDebugCookie();
    configureRavenErrorLogging();

    // We're exposing these functions globally on window because the running app needs the same redirect logic (to display the welcome banner)
    // NOTE: if you change these - please also change ./src/app/shell/window/tm-window.interface.ts
    window.trademe.goToTouchIfNotDev = goToTouchIfNotDev;
    window.trademe.setPreviewPreferenceCookie = setPreviewPreferenceCookie;
    window.trademe.getAuthToken = getAuthToken;
    window.trademe.setAuthToken = setAuthToken;
    window.trademe.readCookie = readCookie;

    try {
        localStorage.setItem('a', 'a');
        localStorage.removeItem('a');
    } catch(e) {
        // TODO handle this: NGT-2562
        console.error('Browser does not support localStorage.');
    }
    window.trademe.localStorageInMemoryFallback = {};
    var localStorageWithFallback = {
        getItem: getFromLocalStorageOrInMemoryFallback,
        setItem: setToLocalStorageOrInMemoryFallback
    };
    window.trademe.localStorageWithFallback = localStorageWithFallback;

    var AUTH_HEADER_NAME = 'authorization';
    var AUTH_TOKEN_STORAGE_KEY = 'trademe.AuthToken';
    var AUTH_TOKEN_URL_PARAM_KEY = '#_h_=';
    var ORIGINAL_REFERRER_STORAGE_KEY = 'trademe.OriginalReferrer';
    var PREFERENCE_COOKIE = 'previewPreferenceAB';
    var UID_HEADER_NAME = 'x-trademe-uniqueclientid';

    var preferenceCookie = readCookie(PREFERENCE_COOKIE);
    if (preferenceCookie) {
        if (preferenceCookie === 'forceTouch' || preferenceCookie === 'neverPreview') {
            window.location = previewToTouch(window.location.href);
            return;
        } else if (preferenceCookie === 'preferTouch' || preferenceCookie === 'notSet') {
            setPreviewPreferenceCookie('preferPreview');
        } else if (preferenceCookie === 'forcePreview') {
            // Reverts AB users
            var bannerCookie = readCookie('remove-welcome-banner');
            if (bannerCookie) {
                // Delete the remove banner cookie if it's set.
                // If people switch preference we want them to see the banner for now.
                writeTmCookie('remove-welcome-banner', '', 'Sun, 30 Jan 2011 00:20:30 GMT', true);
            }
            setPreviewPreferenceCookie('notSet');
            window.location = previewToTouch(window.location.href);
            return;
        }
    } else {
        if (isBadDevice()) {
            setPreviewPreferenceCookie('neverPreview');
            window.location = previewToTouch(window.location.href);
            return;
        } else {
            setPreviewPreferenceCookie('preferPreview');
        }
    }

    // Set the referrer on the original app load, we will remove it on first route in the app
    // so that we can tell if the current page is first page of the session. I know.
    localStorageWithFallback.setItem(ORIGINAL_REFERRER_STORAGE_KEY, document.referrer || 'direct');

    var uid = readCookie(UID_HEADER_NAME) || makeAndSaveUniqueId();

    var authToken = getAuthToken();

    var handoverToken;
    if (location.hash && location.hash.indexOf(AUTH_TOKEN_URL_PARAM_KEY) === 0) {
        handoverToken = location.hash.replace(AUTH_TOKEN_URL_PARAM_KEY, '');
        location.hash = '';
        if ('replaceState' in history) {
            history.replaceState('', document.title, window.location.href.substr(0, window.location.href.indexOf('#')));
        }
    }

    if (handoverToken) {
        if (!authToken) {
            swapHandoverTokenForJWT(handoverToken, function (response) {
                if (response.Token) {
                    setAuthToken(response.Token);
                }
                authToken = response.Token;
                doBootstrap();

                // And again clean up after ourselves now we're finished with the temp token.
                invalidateHandoverToken(handoverToken);
            });
        } else {
            doBootstrap();
            // We always invalidate the temporary token even if we didn't use it.
            invalidateHandoverToken(handoverToken);
        }
    } else {
        doBootstrap();
    }

    function configureRavenErrorLogging () {
        var disableErrorLogging = env === 'dev'
            || window.location.search.indexOf('disableErrorLogging') !== -1
            || window.trademe.debugInfo.disableSentry;

        if (!disableErrorLogging) {
            var scriptEl = document.createElement('script');
            scriptEl.onload = function () {
                Raven.config('https://8c78da77550a4d38b342c1c7d7abec1d@sentry.io/169780', {
                    stacktrace: true,
                    environment: env
                }).install();
            };
            scriptEl.setAttribute('src', ravenScriptSrc);
            document.body.appendChild(scriptEl);
        }
    }

    function doBootstrap () {
        if (window.bootstrapFrEnd) {
            // In production we manually call this bootstrap fn. In dev, the files just run.
            window.bootstrapFrEnd();
            window.bootstrapFrEnd = undefined;
        }
    }

    function previewToTouch (url) {
        // Rules are run in this order!
        var rewriteRules = [
            function () {
                var listingDetailsRegexExec = /.*[\/.*\/]+(\d+)/.exec(url); // marketplace/foo/bar/123456
                this.applies = function () {
                    return !!listingDetailsRegexExec;
                };
                this.modifyUrl = function () {
                    url = window.location.origin + '/listing/view/' + listingDetailsRegexExec[1];
                };
            },
            function () {
                var searchRegexExec = /(?:.+)marketplace\/search\/(.+)/g.exec(url); // marketplace/search/category=123/foo=bar
                this.applies = function () {
                    return !!searchRegexExec;
                };
                this.modifyUrl = function () {
                    url = window.location.origin + '/search/' + searchRegexExec[1].split('/').join('+');
                };
            },
            function () {
                this.applies = function () {
                    return true;
                };
                this.modifyUrl = function () {
                    url = url.replace('preview.', 'touch.');
                };
            }
        ];

        rewriteRules.forEach(function(Rule) {
            var rule = new Rule();
            if (rule.applies()) {
                rule.modifyUrl();
            }
        });

        return url;
    }

    function isBadDevice () {
        var userAgent = navigator.userAgent;

        if (!userAgent) {
            return true;
        }
        //Blackberry
        if (/BB10/i.test(userAgent)) {
            return true;
        }
        //Windows Phone except Lumia 950
        if (/windows phone/i.test(userAgent) && !(/lumia 950/i.test(userAgent))) {
            return true;
        }
        //Not at least Android 4.2
        //Windows Phone has a user agent string of Android 4.2.1 because of course it does.
        if ((userAgent.indexOf("Android") >= 0) && !(/lumia 950/i.test(userAgent))) {
            var androidversion = parseFloat(userAgent.slice(userAgent.indexOf("Android") + 8));
            if (androidversion <= 4.2) {
                return true;
            }
        }
        //Android browser
        var isNativeAndroidBrowser =
            ((userAgent.indexOf('Mozilla/5.0') > -1
            && userAgent.indexOf('Android ') > -1
            && userAgent.indexOf('AppleWebKit') > -1)
            && (userAgent.indexOf('Version') > -1)
            && !(userAgent.indexOf('Chrome') > -1));
        if (isNativeAndroidBrowser) {
            return true;
        }

        return false;
    }

    function getFromLocalStorageOrInMemoryFallback (key) {
        var value;
        try {
            value = localStorage.getItem(key);
        } catch (e) {
            value = window.trademe.localStorageInMemoryFallback[key];
        }
        return value;
    }

    function setToLocalStorageOrInMemoryFallback (key, value) {
        try {
            localStorage.setItem(key, value);
        } catch (e) {
            window.trademe.localStorageInMemoryFallback[key] = value;
        }
    }

    function readCookie (name) {
        try {
            var c, i;
            var cookies = document.cookie.split(';') || [];
            for (i = 0; i < cookies.length; i++) {
                if (cookies[i].split('=')[0].trim() === name) {
                    c = cookies[i];
                    break;
                }
            }
            return c
                ? c.split('=')[1].trim()
                : null;
        } catch (e) {
            console.error('Failed to read cookie. This probably means there was a malformed cookie.');
            return null;
        }
    }

    function writeTmCookie (name, value, expiry, isSecure) {
        var tmCookie = name + '=' + value + ';domain=.trademe.co.nz;path=/;expires=' + expiry;
        if (isSecure) { tmCookie += ';secure'; }
        document.cookie = tmCookie;
    }

    function setPreviewPreferenceCookie (value) {
        writeTmCookie(PREFERENCE_COOKIE, value, 'Tue, 30 Jan 2018 00:04:01 GMT', false);
    }

    function makeAndSaveUniqueId () {
        var guid = makeGuid();
        var expiry = new Date();
        expiry.setDate(expiry.getDate() + (365 * 99));
        writeTmCookie(UID_HEADER_NAME, guid, expiry.toGMTString(), true);
        return guid;
    }

    function makeGuid () {
        function s4() { return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1); }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function getAuthToken () {
        var token = localStorageWithFallback.getItem(AUTH_TOKEN_STORAGE_KEY);
        try {
            // Sometimes our token gets stored as a string with an extra set of quotes baked in. So we try and parse that
            // out to be an actual string.
            token = JSON.parse(token);
        } catch (e) {}
        return token;
    }

    function setAuthToken (token) {
        localStorageWithFallback.setItem(AUTH_TOKEN_STORAGE_KEY, token);
    }

    function goToTouchIfNotDev () {
        if (isDev) {
            console.warn('Touch redirect suppressed because running in dev.');
        } else {
            setPreviewPreferenceCookie('preferTouch');
            window.location = previewToTouch(window.location.href);
        }
    }

    function swapHandoverTokenForJWT (handoverToken, onSuccess) {
        var xhr = new XMLHttpRequest();
        var payload = {
            TargetConsumerKey: '0B3026DED359583EDAC7DF3654CA3927',
            TargetConsumerSecret: '',
            TargetTokenType: 1  // JWT
        };
        xhr.open('POST', '/ngapi/SwapToken.json', true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.setRequestHeader(AUTH_HEADER_NAME, 'Bearer ' + handoverToken);
        xhr.onreadystatechange = function () {
            if (this.readyState == XMLHttpRequest.DONE) {
                if (this.status === 200) {
                    var response = JSON.parse(this.responseText);
                    onSuccess(response);
                } else {
                    setAuthToken(null);
                }
            }
        };
        xhr.send(JSON.stringify(payload));
        return xhr;
    }

    function invalidateHandoverToken (handoverToken) {
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', '/ngapi/v1/authentication/token.json', true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.setRequestHeader(AUTH_HEADER_NAME, 'Bearer ' + handoverToken);
        xhr.send();
        return xhr;
    }

    function readDebugCookie () {
        var debugInfo = {};
        try {
            var debugCookieStr = readCookie('__tm_debug');
            if (debugCookieStr) {
                debugInfo = JSON.parse(debugCookieStr);
            }
        } catch (e) {
            console.error('Failed to json parse cookie stuff, ', e);
        }
        window.trademe.debugInfo = debugInfo;
    }
})();
