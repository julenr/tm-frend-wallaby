import './polyfills.browser.aot';

import { platformBrowser } from '@angular/platform-browser';

import { AppModuleNgFactory } from '../compiled/src/app/app.module.ngfactory';
import { onAppFailedToBoot } from './environment';

export function main () {
    return platformBrowser()
        .bootstrapModuleFactory(AppModuleNgFactory)
        .catch(err => onAppFailedToBoot(err));
}

export function bootstrapDomReady () {
    document.addEventListener('DOMContentLoaded', main);
}

bootstrapDomReady();
