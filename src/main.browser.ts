import './polyfills.browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { decorateModuleRef, onAppFailedToBoot } from './environment';

export function main (): Promise<any> {
    return platformBrowserDynamic()
        .bootstrapModule(AppModule)
        .then(decorateModuleRef)
        .catch(err => onAppFailedToBoot(err));
}

export function bootstrapDomReady ( ) {
    document.addEventListener('DOMContentLoaded', main);
}

bootstrapDomReady();
