import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { APP_IMPORTS } from './app.imports';
import { APP_PROVIDERS } from './app.providers';
import { RootComponent } from './shell/layout/root.component';

/**
 * This module is the entry for your App BROWSER when in UNIVERSAL mode.
 *
 * Make sure to use the 3 constant APP_ imports so you don't have to keep
 * track of your root app dependencies here. Only import directly in this file if
 * there is something that is specific to the environment.
 */

@NgModule({
    declarations: [
        RootComponent
    ],
    imports: [
        APP_IMPORTS,
        BrowserModule // NodeModule, NodeHttpModule, and NodeJsonpModule are included
    ],
    bootstrap: [ RootComponent ],
    providers: [ APP_PROVIDERS ]
})
export class AppModule {
}
