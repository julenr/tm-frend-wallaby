import { Component, OnDestroy, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
// import { AreaOfSiteToStoreLinkService } from '@trademe/core/area-of-site/area-of-site-to-store-link.service';
// import { CurrentMemberService } from '@trademe/core/current-member/current-member.service';
// import { MediaSizeToStoreLinkService } from '@trademe/core/media-size/media-size-to-store-link.service';
// import { RouterToStoreLinkService } from '@trademe/core/router/router-to-store-link.service';
import { CookieService } from 'ngx-cookie';
import { Subject } from 'rxjs/Subject';

// import { InitService } from '../init/init.service';
// import { MobileMenuService } from './mobile-menu/mobile-menu.service';

import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/takeUntil';
import { ObservableStoreItem } from 'src/app/core/ngrx';
import { QueryParamsHandling } from '@angular/router/src/config';

const PREVIEW_PREFERENCE_COOKIE = 'previewPreferenceAB';
const PREVIEW_PREFERENCE_FORCE = 'forcePreview';
const TRADE_ME_BANNER = `%c
 _____              _        __  __
|_   _| __ __ _  __| | ___  |  \\/  | ___    __ _
  | || '__/ _\` |/ _\` |/ _ \\ | |\\/| |/ _ \\  /  ('>--
  | || | | (_| | (_| |  __/ | |  | |  __/  \\__/
  |_||_|  \\__,_|\\__,_|\\___| |_|  |_|\\___|   L\\_
  L i f e     l i v e s     h e r e

`;

/* tslint:disable:component-selector */
@Component({
    selector: 'trade-me',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.scss'],
    encapsulation: ViewEncapsulation.None
})
/* tslint:enable:component-selector */
export class RootComponent implements OnInit, OnDestroy {
    @Input() public listing$: ObservableStoreItem<any>;
    @Input() private queryParamsHandling: QueryParamsHandling;
    public mobileMenuIsOpen: boolean;
    public showWelcomeBanner: boolean;
    public modalActive: boolean;

    public showAffiliates: boolean = true;

    private _destroyed$: Subject<any> = new Subject<any>();
    // private _dummyPropertyToCauseServiceToBeUsed: Array<CurrentMemberService | MediaSizeToStoreLinkService | AreaOfSiteToStoreLinkService | RouterToStoreLinkService> = [];

    constructor (
        private _cookieService: CookieService
        // private _initService: InitService,
        // private _mobileMenuService: MobileMenuService
        // ,
        // areaOfSiteToStoreService: AreaOfSiteToStoreLinkService,
        // currentMemberService: CurrentMemberService,
        // mediaSizeToStoreService: MediaSizeToStoreLinkService,
        // routerToStoreLinkService: RouterToStoreLinkService
    ) {
        // Note: we must inject services here so that they get instantiated at the very top of the dependency tree.
        // This ensures that every other place in the app CMS gets injected, it will be the same instance.
        // We need to "use" the service somehow, otherwise TypeScript will be clever and omit it from the compiled JS.
        // this._dummyPropertyToCauseServiceToBeUsed.concat([
        //     areaOfSiteToStoreService,
        //     currentMemberService,
        //     mediaSizeToStoreService,
        //     routerToStoreLinkService
        // ]);
    }

    public hideMobileMenu (): void {
        // this._mobileMenuService.hide();
    }

    public ngOnInit (): void {
        // this._initService.init();
        this.printConsoleBanner();

        // this._mobileMenuService.isOpen$
        //     .takeUntil(this._destroyed$)
        //     .subscribe(isOpen => {
        //         this.mobileMenuIsOpen = isOpen;
        //     });

        this.checkShowBanner();
    }

    public ngOnDestroy (): void {
        this._destroyed$.next();
        // this._initService.destroy();
    }

    private printConsoleBanner (): void {
        /* tslint:disable-next-line:no-console */
        console.info(TRADE_ME_BANNER, 'font-weight: bold;');
    }

    private checkShowBanner (): void {
        let forcePreview = this._cookieService.get(PREVIEW_PREFERENCE_COOKIE) === PREVIEW_PREFERENCE_FORCE;
        this.showWelcomeBanner = !forcePreview;
    }

    public setModalActive (isModalActive: boolean) {
        this.modalActive = isModalActive;
    }
}
