import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TeleportService } from '@trademe/tangram/services/teleport';
import { TgTestingModule } from '@trademe/tangram/testing';
import { expect, ineeda, sinon } from '@trademe/testing';
import { noop } from '@trademe/testing/test-helpers';
import { CookieService } from 'ngx-cookie';
import { RootComponent } from './root.component';

import { Router } from '@angular/router';

import 'rxjs/add/observable/of';

describe('RootComponent', () => {
    beforeEach(() => {
        // Stub console.info as it logs the ascii banner in this component
        sinon.stub(console, 'info');

        TestBed.configureTestingModule({
            declarations: [
                RootComponent
            ],
            imports: [
                RouterTestingModule,
                TgTestingModule
            ],
            providers: [
                { provide: CookieService, useClass: ineeda.factory<CookieService>({get: noop}) },
                { provide: TeleportService, useClass: ineeda.factory<TeleportService>({ registerOutlet: noop, unregisterOutlet: noop }) }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
    });

    afterEach(() => {
        sinon.restore(console.info);
    });

    it('should create the component', () => {
        // Arrange
        let fixture = TestBed.createComponent(RootComponent);
        let testComponent = fixture.debugElement.componentInstance;

        // Act
        fixture.detectChanges();

        // Assert
        expect(testComponent).to.exist();
        expect(testComponent).to.be.an.instanceof(RootComponent);
    });

    it('should try to deeply compare two objects', () => {
        const obj1 = {a: 1};
        const obj2 = {a: 1};
        expect(require('deep-eql')({a: 1}, {a: 1})).to.be.true();
    });
});
