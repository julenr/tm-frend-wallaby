import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IdlePreload, IdlePreloadModule } from '@angularclass/idle-preload';
// import { AreaOfSiteEnum } from '@trademe/core/area-of-site/area-of-site.enum';

// import { AboutModalComponent } from './shell/layout/welcome-banner/modals/about/about-modal.component';
// import { CategoriesResolver } from './shell/init/api/interceptors/canonical/categories.resolver';
// import { CategoryModule } from './core/category/category.module';
// import { FeedbackModalComponent } from './shell/layout/welcome-banner/modals/feedback/feedback-modal.component';
// import { InitialLoginResolver } from './shell/auth/initial-login.resolver';
// import { LocalitiesResolver } from './shell/init/api/interceptors/canonical/localities.resolver';
// import { LoginModalComponent } from './shell/login-modal/login-modal.component';
// import { LoginModalModule } from './shell/login-modal/login-modal.module';
// import { MobilePanelComponent } from './shell/layout/mobile-panel/mobile-panel.component';
// import { MobileSearchFullScreenComponent } from './shell/layout/mobile-search-full-screen-search/mobile-search-full-screen.component';
// import { NotFoundComponent } from './shell/errors/not-found/not-found.component';
// import { SearchAreasResolver } from './shell/init/api/interceptors/canonical/search-areas.resolver';
// import { SiteErrorPageComponent } from './shell/errors/site-error-page/site-error-page.component';

const appRoutes: Routes = [

];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { preloadingStrategy: IdlePreload }
        ),
        IdlePreloadModule
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
