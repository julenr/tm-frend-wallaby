import { HttpModule } from '@angular/http';
import { IdlePreloadModule } from '@angularclass/idle-preload';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LocalStorageModule } from 'angular-2-local-storage';
import { CookieModule } from 'ngx-cookie';

import { AppRoutingModule } from './app-routing.module';

const debugInfo = (<any>window).trademe.debugInfo;
const STORE_DEV_TOOLS_IMPORTS = [];
const INSTRUMENT_STORE_DEV_TOOLS = debugInfo.forceStoreDevtools || (ENV === 'development' && !AOT);
if (INSTRUMENT_STORE_DEV_TOOLS) {
    STORE_DEV_TOOLS_IMPORTS.push(...[StoreDevtoolsModule.instrument({ maxAge: 50 })]);
}

export const APP_IMPORTS = [
    IdlePreloadModule.forRoot(),
    HttpModule,
    EffectsModule.forRoot([]),
    STORE_DEV_TOOLS_IMPORTS,

    LocalStorageModule.withConfig({ prefix: 'trademe' }),

    CookieModule.forRoot(),

    AppRoutingModule
];
