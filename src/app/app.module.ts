import { ApplicationRef, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_IMPORTS } from './app.imports';
import { APP_PROVIDERS } from './app.providers';
import { RootComponent } from './shell/layout/root.component';
import './trademe.scss';

/**
 * This module is the entry for your App when NOT using universal.
 *
 * Make sure to use the 3 constant APP_ imports so you don't have to keep
 * track of your root app dependencies here. Only import directly in this file if
 * there is something that is specific to the environment.
 */
@NgModule({
    declarations: [
        RootComponent
    ],
    imports: [
        ...APP_IMPORTS,
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule
    ],
    bootstrap: [ RootComponent ],
    providers: [ APP_PROVIDERS ]
})
export class AppModule {
    constructor (
        public appRef: ApplicationRef
    ) { }

    public hmrOnInit (store) {
        if (!store || !store.rootState) {
            return;
        }

        if ('restoreInputValues' in store) {
            store.restoreInputValues();
        }
        this.appRef.tick();
        Object.keys(store).forEach(prop => delete store[prop]);
    }

    public hmrOnDestroy (store) {
        const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    }

    public hmrAfterDestroy (store) {
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }
}
