
// ===================================================================================================
// IMPORTANT!
// ===================================================================================================
// Always use the enum member NAME rather than value when setting localStorage items.
//  e.g. GOOD do this:
// let myKey: string = LocalStorageKeysEnum[LocalStorageKeysEnum.TestToken];
// localStorageService.set(myKey, {good: true});
//
// BAD! Don't do this:
// let myKey: number = LocalStorageKeysEnum.TestToken;
// localStorageService.set(myKey, {good: false);
// This will be storing an entry based on it's enum index (e.g. "0") which means
//   a) the order of items in this enum matters,
//   b) it's not clear when looking at localStorage what the thing is (non descriptive name)
//   c) we could get collisions
export enum LocalStorageKeysEnum {
    MarketplaceLocationRefineRegion,
    CanViewRestrictedListings,
    OriginalReferrer,
    SearchViewModeOverrides,
    JobsSearchFormKeys,
    PropertySearchParams,
    PropertySellState,
    PropertySchoolFilterParams
}
