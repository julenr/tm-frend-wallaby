import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { SafeSvg } from '@trademe/tangram/components/svg/safe-svg';

const ADMIN_ICON_SVG = require('./admin-icon.svg');

@Component({
    selector: 'tm-admin-icon',
    templateUrl: 'admin-icon.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminIconComponent {
    public adminIconSvg = new SafeSvg(ADMIN_ICON_SVG, 'This is definitely a safe string of SVG that did not come from the user.');
}
