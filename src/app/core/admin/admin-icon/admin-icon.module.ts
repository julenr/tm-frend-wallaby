import { NgModule } from '@angular/core';
import { TgSvgModule } from '@trademe/tangram/components/svg/svg.module';

import { AdminIconComponent } from './admin-icon.component';

@NgModule({
    imports: [
        TgSvgModule
    ],
    declarations: [
        AdminIconComponent
    ],
    exports: [
        AdminIconComponent
    ]
})
export class AdminIconModule {}
