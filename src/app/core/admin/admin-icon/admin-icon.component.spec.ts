import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
// import { TgFormsModule } from '@trademe/tangram/components/forms';
import { SafeSvg } from '@trademe/tangram/components/svg/safe-svg';
import { expect } from '@trademe/testing';

import { AdminIconComponent } from './admin-icon.component';

describe('AdminIconComponent', () => {
    let fixture: ComponentFixture<AdminIconComponent>;
    let testComponent: AdminIconComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AdminIconComponent
            ],
            imports: [
                CommonModule,
                FormsModule
                // ,
                // TgFormsModule
            ],
            schemas: [
                NO_ERRORS_SCHEMA
            ]
        });

        fixture = TestBed.createComponent(AdminIconComponent);
        testComponent = fixture.debugElement.componentInstance;
    });

    it('should create the component', () => {
        // Arrange
        // Act
        fixture.detectChanges();

        // Assert
        expect(testComponent).to.exist();
        expect(testComponent).to.be.an.instanceof(AdminIconComponent);
    });

    it('should have defined adminIconSvg', () => {
        // Arrange
        // Act
        fixture.detectChanges();

        // Assert
        expect(testComponent.adminIconSvg).to.be.an.instanceof(SafeSvg);
    });
});
