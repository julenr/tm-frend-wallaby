import { async, inject, TestBed } from '@angular/core/testing';
// import { IMediaSizeMap } from '@trademe/core/media-size/media-size.reducer';
// import { expect, getStoreTestingModule, ineeda, setStoreState, sinon } from '@trademe/testing';
import { expect, getStoreTestingModule, ineeda, sinon } from '@trademe/testing';
import { LocalStorageService } from 'angular-2-local-storage';
import { Observable } from 'rxjs/Observable';

import { CardViewModeService, IViewModeConfigurationByVerticalMap } from './card-view-mode.service';
import { CardViewModeServiceModule } from './card-view-mode.module';
import { LocalStorageKeysEnum } from '../../shared/constants/local-storage-keys.enum';
import { ViewModeEnum } from './card-view-mode.enum';

describe('CardViewModeService', () => {

    const STORAGE_KEY = LocalStorageKeysEnum[LocalStorageKeysEnum.SearchViewModeOverrides];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                getStoreTestingModule(),
                CardViewModeServiceModule.withConfig({
                    vertical: 'test',
                    whenSmall: ViewModeEnum.list,
                    whenNotSmall: ViewModeEnum.card
                })
            ],
            providers: [
                {
                    provide: LocalStorageService,
                    useClass: ineeda.factory<LocalStorageService>()
                }
            ]
        });
    });

    function mockLocalStorageGet (key: string, value: IViewModeConfigurationByVerticalMap = null) {
        let mockStorage = TestBed.get(LocalStorageService);
        sinon.stub(mockStorage, 'get').withArgs(key).returns(value);
        sinon.stub(mockStorage, 'set');
    }

    function setCurrentMediaSize (custom?: Partial<any>) {
        let mediaSize: any = {
            isSm: false, isSd: false,
            isMd: false, isMg: false,
            isLg: false, isLl: false,
            isXl: false
        };
        if (custom) {
            Object.assign(mediaSize, custom);
        }
        // setStoreState({ mediaSize });
    }

    describe('constructor test', () => {
        it('should construct', () => {
            // Arrange
            mockLocalStorageGet(STORAGE_KEY);

            // Act
            let classUnderTest = TestBed.get(CardViewModeService);

            // Assert
            expect(classUnderTest).to.be.an.instanceof(CardViewModeService);
        });
    });
});
