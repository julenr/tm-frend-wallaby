import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Store } from '@ngrx/store';
import { LocalStorageService } from 'angular-2-local-storage';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

// import { ICoreState } from '../../core/reducers';
// import { LocalStorageKeysEnum } from '../../shared/constants/local-storage-keys.enum';
// import { MediaSizeSelectors } from '../../core/media-size/media-size.selectors';
import { ViewModeEnum } from './card-view-mode.enum';

import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/observable/combineLatest';

/** View mode overrides for specific media breakpoints */
export interface IViewModeConfiguration {
    whenSmall: ViewModeEnum;
    whenNotSmall: ViewModeEnum;
}

/** Per-vertical view mode overrides for specific media breakpoints  */
export interface IVerticalViewModeConfiguration extends IViewModeConfiguration {
    vertical: string;
}

/** Vertical configuration hashmap (stashed in local storage) */
export interface IViewModeConfigurationByVerticalMap {
    [vertical: string]: IViewModeConfiguration;
}

const STORAGE_KEY = 'MOCK';

/** The initial user overrides */
const DEFAULT_OVERRIDES: IViewModeConfiguration = {
    whenSmall: null,
    whenNotSmall: null
};

export const CARD_VIEW_MODE_SERVICE_CONFIG_TOKEN = new InjectionToken<IVerticalViewModeConfiguration>('CardViewModeServiceConfig');

// TODO: Awaiting fix https://github.com/TradeMe/ensure/issues/2
//       Will allow us to run multiple tests without singleton error
// @Singleton()

@Injectable()
export class CardViewModeService {

    private _overrideSubject: BehaviorSubject<IViewModeConfiguration>;

    /**
     * Represents the current view mode that should be used.
     * Values are selected based on the current media breakpoint, the vertical's configuration
     * and the user's previously stored preferences.
     */
    public viewModeChange$: Observable<ViewModeEnum>;

    // tslint complaining about @Inject() decorator
    // tslint:disable-next-line:constructor-params-format
    constructor (
        private _localStorageService: LocalStorageService,
        private _store: Store<any>,
        @Inject(CARD_VIEW_MODE_SERVICE_CONFIG_TOKEN) private _config: IVerticalViewModeConfiguration
    ) {
        this._overrideSubject = new BehaviorSubject<IViewModeConfiguration>(this.loadViewModeOverrides());
        this.viewModeChange$ =
            this._store.select('large')
                .combineLatest(this._overrideSubject, (sizeMap, override) => {
                    if (sizeMap.isSm) {
                        return (override.whenSmall !== null) ? override.whenSmall : this._config.whenSmall;
                    } else {
                        return (override.whenNotSmall !== null) ? override.whenNotSmall : this._config.whenNotSmall;
                    }
                })
                .distinctUntilChanged();
    }

    private loadViewModeOverrides (): IViewModeConfiguration {
        let map = this._localStorageService.get<IViewModeConfigurationByVerticalMap>(STORAGE_KEY);
        let value = map && map[this._config.vertical];
        return value ? value : DEFAULT_OVERRIDES;
    }

    private storeViewModeOverrides (value: IViewModeConfiguration) {
        let map = this._localStorageService.get<IViewModeConfigurationByVerticalMap>(STORAGE_KEY) || {};
        map[this._config.vertical] = value;
        this._localStorageService.set(STORAGE_KEY, map);
    }

    public overrideUsersViewModePreferenceForCurrentMediaBreakpoint (preferredViewMode: ViewModeEnum) {
        // HACK: Our interface sets the preference for the CURRENT media breakpoint and I do not want to
        // track that state manually. Directly grab the latest state from the store instead (cold observable).
        this._store.select('large')
            .subscribe(sizeMap => {
                let currentOverrides = this._overrideSubject.value;
                let newOverrides =
                    sizeMap.isSm
                        ? { ...currentOverrides, whenSmall: preferredViewMode }
                        : { ...currentOverrides, whenNotSmall: preferredViewMode };

                this.storeViewModeOverrides(newOverrides);
                this._overrideSubject.next(newOverrides);
            })
            .unsubscribe();
    }
}
