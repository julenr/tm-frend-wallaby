import { ModuleWithProviders, NgModule } from '@angular/core';

import {
    CARD_VIEW_MODE_SERVICE_CONFIG_TOKEN,
    CardViewModeService,
    IVerticalViewModeConfiguration
} from './card-view-mode.service';

@NgModule({
    providers: [
        CardViewModeService
    ]
})
export class CardViewModeServiceModule {
    public static withConfig (config: IVerticalViewModeConfiguration): ModuleWithProviders {
        return {
            ngModule: CardViewModeServiceModule,
            providers: [
                { provide: CARD_VIEW_MODE_SERVICE_CONFIG_TOKEN, useValue: config }
            ]
        };
    }
}
