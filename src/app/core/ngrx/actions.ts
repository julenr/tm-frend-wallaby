import { Action } from '@ngrx/store';

function ActionBaseFactory (actionType: string = 'build-problem-this-should-be-generated') {

    ensureUniqueType(actionType);

    abstract class ActionBaseCls implements Action {
        public static readonly TYPE = actionType;
        public readonly type = actionType;
    }
    return ActionBaseCls;
}

export const ActionBase = ActionBaseFactory;

const typeCache: { [label: string]: boolean } = {};
function ensureUniqueType (label: string): void {
    if (typeCache[label]) {
        // If you are reading this ebcause you are seeing 'build-problem-this-should-be-generated' being thrown, it likely means
        // somethign has gone wrong in the webpack config. At build time, there is an "action-context-loader" that rewrites all
        // actions so they definitely pass in a value to the ActionBaseFactory.

        // Construct a new error so we can parse the stacktrace and try provide more context for the error we're about to throw
        const errorForStackTrace = new Error();
        const stackLine = errorForStackTrace.stack.split('\n').find(line => line.indexOf('.actions.ts') !== -1);

        throw new Error(`Action type "${label}" is not unique.
This probably means you have copy pasted an Action and not renamed it correctly.
${stackLine}`);
    }

    typeCache[label] = true;
}
