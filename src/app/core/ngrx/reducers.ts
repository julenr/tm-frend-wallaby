import { Action } from '@ngrx/store';

export type Reducer<T> = (state: T, action?: Action) => T;

export type ReducerMap<T> = {
    [actionType: string]: Reducer<T>
};

export function applyReducers<T> (actionHandlers: ReducerMap<T>, initialState?: T): Reducer<T> {
    return (state: T = initialState, action: Action) => {
        const handler = actionHandlers[action.type];
        return handler ? handler(state, action) : state;
    };
}
