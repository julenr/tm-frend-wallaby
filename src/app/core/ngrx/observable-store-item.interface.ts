import { Observable } from 'rxjs/Observable';

import { IStoreItem } from './store-item.interface';

/*tslint:disable:interface-name*/
export interface ObservableStoreItem<T> extends Observable<IStoreItem<T>> {
}
