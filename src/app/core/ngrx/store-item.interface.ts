export interface IStoreItem<T> {
    isLoading: boolean;
    error?: Error;
    cachedAt?: Date;
    item?: T;
}
