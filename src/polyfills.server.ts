import 'core-js/es6/reflect';
import 'core-js/es7/reflect';
import 'delayed-scroll-restoration-polyfill';

const promiseFinally = require('promise.prototype.finally');
promiseFinally.shim();
