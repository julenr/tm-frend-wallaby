import 'core-js/es6/reflect';
import 'core-js/es7/reflect';
import 'core-js/client/shim';
import 'zone.js/dist/zone';
import 'zone.js/dist/zone-error';
import 'zone.js/dist/long-stack-trace-zone';
import 'intl';
import 'intl/locale-data/jsonp/en.js';
import 'delayed-scroll-restoration-polyfill';

const promiseFinally = require('promise.prototype.finally');
promiseFinally.shim();
