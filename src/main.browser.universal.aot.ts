import './polyfills.browser.aot';
import { platformUniversalDynamic } from 'angular2-universal';

import { AppModuleNgFactory } from '../compiled/src/app/app.module.universal.browser.ngfactory';
import { onAppFailedToBoot } from './environment';

export function main () {
    return platformUniversalDynamic()
        .bootstrapModuleFactory(AppModuleNgFactory)
        .catch(err => onAppFailedToBoot(err));
}

export function bootstrapDomReady () {
  document.addEventListener('DOMContentLoaded', main);
}

bootstrapDomReady();
