import './polyfills.browser';
import { platformServer } from '@angular/platform-server';

import { AppModule } from './app/app.module.universal.browser';
import { onAppFailedToBoot } from './environment';

export function main () {
    return platformServer()
        .bootstrapModule(AppModule)
        .catch(err => onAppFailedToBoot(err));
}

export function bootstrapDomReady () {
  document.addEventListener('DOMContentLoaded', main);
}

bootstrapDomReady();
