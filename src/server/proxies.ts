const proxy = require ('express-http-proxy');

const TM_ENV = 'test1a'; // TODO allow configuring env instead of hardcoding

export function registerProxies (app) {
    const proxyMappings = [
        { from: '/ngapi', to: `api.${TM_ENV}.trademe.co.nz` },
        { from: '/images/', to: `${TM_ENV}.trademe.co.nz` }
    ];

    proxyMappings.forEach(mapping => {
        app.use(mapping.from, proxy(mapping.to, {
            decorateRequest: function (proxyReq, originalReq) {
                proxyReq.headers.host = mapping.to;
                proxyReq.headers.referer = `http://${mapping.to}`;
                return proxyReq;
            },
            forwardPath: function (req, res) {
                return mapping.from + require('url').parse(req.url).path;
            },
            limit: '10mb'
        }));
    });
}
