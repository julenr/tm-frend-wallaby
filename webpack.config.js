const {
    ContextReplacementPlugin,
    DefinePlugin,
    DllReferencePlugin,
    ProgressPlugin,
    NamedModulesPlugin
} = require('webpack');
const AutoPrefixer = require('autoprefixer');
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');

const { root, testDll, resolveToScss } = require('./helpers.js');
const { AUTOPREFIXER_BROWSERS, DEV_PORT, EXCLUDE_SOURCE_MAPS, HOST, PROTOCOL, STORE_DEV_TOOLS } = require('./constants.js');

const CONSTANTS = {
    AOT: false,
    ENV: JSON.stringify('development'),
    HMR: false,
    HOST: JSON.stringify(HOST),
    PORT: DEV_PORT,
    STORE_DEV_TOOLS: JSON.stringify(STORE_DEV_TOOLS),
    UNIVERSAL: false,
};

testDll();

console.log(`
    Building FrEnd ...

    Starting dev server on: ${PROTOCOL}://${HOST}:${DEV_PORT}`);

module.exports = function(env) { // pass --env options from webpack
    const config = {
        resolve: {
            extensions: ['.ts', '.js', '.json'],
            alias: {
                '@trademe/core': root('src/app/core'),
                '@trademe/testing': root('test-config')
            }
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    enforce: 'pre',
                    loader: 'source-map-loader',
                    exclude: EXCLUDE_SOURCE_MAPS
                },
                {
                    test: /\.ts$/,
                    loaders: [
                        '@angularclass/hmr-loader',
                        'awesome-typescript-loader?{configFileName: "tsconfig.webpack.json"}',
                        'angular2-template-loader',
                        'angular-router-loader?loader=system&aot=false'
                    ],
                    exclude: [/\.(spec|e2e|d)\.ts$/]
                },
                { test: /\.html/, loader: 'raw-loader', exclude: [root('src/index.html')] },
                {
                    test: /\.(scss|css)$/,
                    include: root('src/app/trademe.scss'),
                    use: ['to-string-loader'].concat(ExtractTextPlugin.extract({
                        use: [
                            {
                                loader: 'css-loader',
                                options: { sourceMap: true, url: false }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                    plugins: [AutoPrefixer({ browsers: AUTOPREFIXER_BROWSERS })]
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true,
                                    includePaths: [root('node_modules')],
                                    importer: resolveToScss
                                }
                            }
                        ],
                    }))
                },
                {
                    test: /\.(scss|css)$/,
                    exclude: root('src/app/trademe.scss'),
                    use: [
                        {
                            loader: 'to-string-loader', options: { sourceMap: true }
                        },
                        {
                            loader: 'css-loader',
                            options: { sourceMap: true, url: false }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                plugins: [AutoPrefixer({ browsers: AUTOPREFIXER_BROWSERS })]
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                                includePaths: [root('node_modules')],
                                importer: resolveToScss
                            }
                        }
                    ],
                },
                { test: /\.svg/, loader: 'svg-inline-loader?removeSVGTagAttrs=false' },
            ],
        },
        plugins: [
            new ProgressPlugin(),
            new CheckerPlugin(),
            new DefinePlugin(CONSTANTS),
            new NamedModulesPlugin(),
            new TsConfigPathsPlugin(),
            new ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, './src', {}),
            new DllReferencePlugin({
                context: '.',
                manifest: require(`./dll/polyfill-manifest.json`)
            }),
            new DllReferencePlugin({
                context: '.',
                manifest: require(`./dll/vendor-manifest.json`)
            }),
            new ExtractTextPlugin({ filename: 'trademe.[contenthash].css', allChunks: true }),
            new CopyWebpackPlugin([
                { from: root('dll') },
                { from: root('src/assets/dist_root') },
                { from: root('src/favicon.ico'), force: true },
                { from: root('node_modules/raven-js/dist/raven.min.js'), to: 'raven.[hash].min.js' }
            ], { copyUnmodified: true }),
            new HtmlWebpackPlugin({
                template: root('src/index.html.ejs'),
                manuallyIncludeAssets: true,
                inject: false
            }),
        ],
        cache: true,
        devtool: 'eval-source-map',
        entry: {
            index: './src/main.browser.ts'
        },
        output: {
            path: root('dist/client'),
            filename: 'index.js'
        },
        watch: true,
        devServer: {
            contentBase: './src',
            port: CONSTANTS.PORT,
            https: true,
            historyApiFallback: {
                disableDotRule: true
            },
            host: HOST,
            watchOptions: {
                poll: undefined,
                aggregateTimeout: 3000,
                ignored: [/node_modules/, /\.awcache/]
            },
            proxy: {
            },
            setup (app) {
                app.use((req, res, next) => {
                    // Enforce devving against preview.dev.trademe.co.nz, so our environment specific code runs, and our CanaryTokens don't trigger
                    if (!req.hostname || !req.hostname.match(/trademe\.co\.nz$/)) {
                        res.writeHead(404, { 'Content-Type': 'text/plain' });
                        res.end('404 Not found.\r\nCannot dev against localhost - make sure you have a hostfile entry setup for preview.dev.trademe.co.nz');
                    } else {
                        next();
                    }
                });
            },
            stats: {
                assets: true,
                cached: false,
                cachedAssets: false,
                children: false,
                chunks: false,
                chunkModules: false,
                chunkOrigins: false,
                colors: true,
                context: root('./src'),
                depth: false,
                entrypoints: false,
                errors: true,
                errorDetails: false,
                hash: false,
                maxModules: 0,
                modules: false,
                moduleTrace: false,
                performance: true,
                providedExports: false,
                publicPath: false,
                reasons: false,
                timings: true,
                usedExports: false,
                version: true,
                warnings: true
            },
            overlay: {
                errors: true,
                warnings: false
            }
        },
        performance: {
            hints: false
        },
        node: {
            global: true,
            process: true,
            Buffer: false,
            crypto: true,
            module: false,
            clearImmediate: false,
            setImmediate: false,
            clearTimeout: true,
            setTimeout: true
        }
    };
    if (env && env.lint) {
        console.log(' linting enabled');

        config.module.rules.unshift({
            test: /\.ts$/,
            enforce: 'pre',
            loader: 'tslint-loader',
            include: root('./src'),
            options: {
                configFile: root('tslint.json'),
                emitErrors: false,
                failOnHint: false
            }
        });
    }

    let proxies = [];

    return config;
};
