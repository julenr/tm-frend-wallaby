import { Component, Pipe } from '@angular/core';
import * as _sinon from 'sinon';

export { expect } from 'chai';

export { ineeda } from 'ineeda';

export function noop (): any { return null; }

export const sinon = _sinon;

export function createMockComponent (selector: string, template: string = '') {
    @Component({ selector, template })
    class MockComponent {}
    return MockComponent;
}

export function createMockPipe (name: string) {
    @Pipe({ name })
    class MockPipe {
        transform (): string {
            return;
        }
    }
    return MockPipe;
}

