const {
    ContextReplacementPlugin,
    DefinePlugin,
    NamedModulesPlugin,
    ProgressPlugin
} = require('webpack');
const { root } = require('../helpers');
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader');
const FocusTestWebpackPlugin = require('../_build/webpack/focus-test-webpack-plugin');

const { EXCLUDE_SOURCE_MAPS, STORE_DEV_TOOLS } = require('../constants');

const ENV = process.env.npm_lifecycle_event || '';
const TEST_ONCE = ENV.indexOf('test:once') !== -1;
const TEST_PROD = ENV.indexOf('test:prod') !== -1;
if (TEST_ONCE) {
    console.log('Test Once');
}
if (TEST_PROD) {
    console.log('Running Prod Unit Tests');
}

const COVERAGE_IS_ENABLED = TEST_ONCE || TEST_PROD;
const STOP_ON_ERRORS = TEST_ONCE || TEST_PROD;
const FOCUS_TESTS_ENABLED = !TEST_ONCE && !TEST_PROD;
if (FOCUS_TESTS_ENABLED) {
    console.log('Focus Tests Enabled');
}

const CONSTANTS = {
    AOT: false,
    ENV: JSON.stringify('test'),
    HMR: false,
    PORT: 3000,
    HOST: JSON.stringify('localhost'),
    STORE_DEV_TOOLS: JSON.stringify(STORE_DEV_TOOLS),
    UNIVERSAL: false
};

let config = {
    /**
     * Source map for Karma from the help of karma-sourcemap-loader &  karma-webpack
     *
     * Do not change, leave as is or it wont work.
     * See: https://github.com/webpack/karma-webpack#source-maps
     */
    devtool: 'inline-source-map',

    cache: true,

    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            '@trademe/core': root('src/app/core'),
            '@trademe/testing': root('test-config')
        }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                enforce: 'pre',
                loader: 'source-map-loader',
                // exclude ineeda source maps until fixed
                exclude: EXCLUDE_SOURCE_MAPS
            },
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            sourceMap: false,
                            inlineSourceMap: true,
                            removeComments: true,
                            useCache: true
                        }
                    },
                    { loader: 'angular2-template-loader' }
                ],
                exclude: [/\.(e2e|d)\.ts$/, root('node_modules/@angular'), root('node_modules/rxjs')]
            },

            // Skip loading of scss and css files for tests
            { test: /\.(scss|css)$/, loaders: ['to-string-loader', 'null-loader'] },

            { test: /\.html$/, loader: 'raw-loader' },

            { test: /\.svg/, loaders: ['to-string-loader', 'null-loader'] }
        ]
    },

    plugins: [
        new CheckerPlugin(),
        // NOTE: when adding more properties make sure you include them in custom-typings.d.ts
        new DefinePlugin(CONSTANTS),
        new NamedModulesPlugin(),
        new TsConfigPathsPlugin(),
        new ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, root('./src'), {}),
        new FocusTestWebpackPlugin({ enabled: FOCUS_TESTS_ENABLED })
    ],

    node: {
        fs: 'empty',
        global: true,
        crypto: 'empty',
        tls: 'empty',
        net: 'empty',
        process: true,
        module: false,
        clearImmediate: false,
        setImmediate: false
    }
};

if (!TEST_PROD) {
    config.plugins.unshift(new ProgressPlugin());
    config.stats = { // Also prevent chunk and module display output, cleaner look. Only emit errors.
        assets: false,
        cached: false,
        cachedAssets: false,
        children: false,
        chunks: false,
        chunkModules: false,
        chunkOrigins: false,
        colors: true,
        context: root('./src'),
        depth: false,
        entrypoints: false,
        errors: true,
        errorDetails: true,
        hash: false,
        maxModules: 0,
        modules: false,
        moduleTrace: false,
        performance: false,
        providedExports: false,
        publicPath: false,
        reasons: false,
        timings: false,
        usedExports: false,
        version: true,
        warnings: true
    };
}

if (COVERAGE_IS_ENABLED) {
    config.module.rules.push(
        {
            test: /\.(js|ts)$/,
            loader: 'istanbul-instrumenter-loader',
            enforce: 'post',
            include: root('src'),
            exclude: [/\.(e2e|spec)\.ts$/, /node_modules/]
        }
    );
}

if (STOP_ON_ERRORS) {
    config.plugins.push(
        {
            apply: (compiler) => {
                compiler.plugin('done', (stats) => {
                    if (stats.compilation.errors.length > 0) {
                        throw new Error(stats.compilation.errors.map((err) => err.message || err));
                    }
                });
            }
        }
    );
}

module.exports = config;
