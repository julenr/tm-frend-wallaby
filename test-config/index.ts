import { OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActionReducer, Store, StoreModule } from '@ngrx/store';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as dirtyChai from 'dirty-chai';
import { ineeda } from 'ineeda';
import { Observable } from 'rxjs/Observable';
import { Scheduler } from 'rxjs/Scheduler';
import * as _sinon from 'sinon';
import * as sinonChai from 'sinon-chai';

export { expect } from 'chai';
export { ineeda } from 'ineeda';
export function noop (): any { return null; }
export * from 'rxjs/BehaviorSubject';
export * from 'rxjs/Observable';
export const sinon = _sinon;

// Typed wrapper for sinon.stub
export function stub (): _sinon.SinonStub;
export function stub <T> (instance: T): _sinon.SinonStub;
export function stub <T> (instance: T, method: keyof T): _sinon.SinonStub;
export function stub (instance?, method?) {
    return sinon.stub(instance, method);
}

// Typed wrapper for sinon.spy
export function spy (): _sinon.SinonStub;
export function spy (func: Function): _sinon.SinonStub;
export function spy <T> (instance: T, method: keyof T): _sinon.SinonStub;
export function spy (funcOrInstance?, method?) {
    return sinon.spy(funcOrInstance, method);
}

// In unit tests we use the real store, but have it hooked up to a fake noop reducer
export function getStoreTestingModule () {
    return StoreModule.forRoot({}, {
        initialState: {},
        metaReducers: [
            function (_: ActionReducer<any>): ActionReducer<any> {
                return function (state = {}, action?: any) {
                    if (action && action.type === 'SET_STATE_FOR_TESTS') {
                        return action.payload || {};
                    }
                    return state;
                };
            }
        ]
    });
}

/**
 * Simulates an Angular template setting inputs on a component T and triggering change detection and ngOnChanges.
 */
export function simulateComponentInput<T extends OnChanges> (fixture: ComponentFixture<T>, inputValues: Partial<T> = {}) {
    let instance = fixture.componentInstance;
    let changes: SimpleChanges = {};
    for (let prop in inputValues) {
        if (inputValues.hasOwnProperty(prop)) {
            changes[prop] = new SimpleChange(instance[prop], inputValues[prop], !(prop in instance));
            instance[prop] = inputValues[prop];
        }
    }
    fixture.detectChanges();
    instance.ngOnChanges(changes);
}

// This function is used to setup the store state in tests
type Partial<T> = {
    [P in keyof T]?: Partial<T[P]>;
};

// The order of these is important:
chai.use(sinonChai);
chai.use(chaiAsPromised);
chai.use(dirtyChai);

// Prevent zone from thinking ineeda mocks are Promises:
ineeda.intercept<Promise<any>>(Promise, { then: null });
// Prevent RxJS from thinking ineeda mocks are Schedulers:
ineeda.intercept<Scheduler>(Observable, { schedule: null });
// Prevent sinon from thinking ineeda mocks are already stubbed:
ineeda.intercept({ restore: null, calledBefore: null });
// Prevent zone from thinking ineeda mocks are unconfigurable:
ineeda.intercept({ __zone_symbol__unconfigurables: null });
// Prevent deep-equal from thinking ineeda mocks are Buffers:
ineeda.intercept<Buffer>(Buffer, { copy: null });
