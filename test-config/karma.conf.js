const testWebpackConfig = require('./webpack.test.js');

const ENV = process.env.npm_lifecycle_event || '';
const TEST_ONCE = ENV.indexOf('test:once') !== -1;
const TEST_PROD = ENV.indexOf('test:prod') !== -1;

const RUN_ONCE = TEST_ONCE || TEST_PROD;
const COVERAGE_IS_ENABLED = TEST_ONCE || TEST_PROD;
const USE_TEAMCITY_REPORTER = TEST_PROD && ('TEAMCITY_VERSION' in process.env);

const configuration = {

    // base path that will be used to resolve all patterns (e.g. files, exclude)
    basePath: '',

    /*
     * Frameworks to use
     *
     * available frameworks: https://npmjs.org/browse/keyword/karma-adapter
     */
    frameworks: ['jasmine'],

    // list of files to exclude
    exclude: [],

    /*
     * list of files / patterns to load in the browser
     *
     * we are building the test environment in ./spec-bundle.js
     */
    files: [
        { pattern: './test-config/spec-bundle.js', watched: false }
    ],

    /*
     * preprocess matching files before serving them to the browser
     * available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
     */
    preprocessors: { './test-config/spec-bundle.js': ['webpack', 'sourcemap'] },

    // Webpack Config at ./webpack.test.js
    webpack: testWebpackConfig,

    // Webpack please don't spam the console when running in karma!
    webpackServer: { noInfo: true },

    /*
     * test results reporter to use
     *
     * possible values: 'dots', 'progress'
     * available reporters: https://npmjs.org/browse/keyword/karma-reporter
     */
    reporters: [USE_TEAMCITY_REPORTER ? 'teamcity' : 'mocha'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: !RUN_ONCE,

    /*
     * start these browsers
     * available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
     */
    browsers: [TEST_PROD ? 'Electron' : 'Chrome_no_background_throttling'],
    browserNoActivityTimeout: 30000,

    customLaunchers: {
        Chrome_no_background_throttling: {
            base: 'Chrome',
            flags: ['--disable-background-throttling', '--disable-background-timer-throttling']
        },
    },

    /*
     * Continuous Integration mode
     * if true, Karma captures browsers, runs the tests and exits
     */
    singleRun: RUN_ONCE,
    /*
    Don't show all the skipped tests in the console - the total number still shows in the summary
     */
    mochaReporter: {
        ignoreSkipped: true
    }
};

if (COVERAGE_IS_ENABLED) {
    configuration.coverageReporter = {
        type: 'in-memory'
    };

    configuration.remapCoverageReporter = {
        'text-summary': null,
            json: './reports/coverage/coverage.json',
            html: './reports/coverage/html'
    };

    configuration.reporters.push('coverage');
    configuration.reporters.push('remap-coverage');
}

module.exports = function(config) {
    /*
     * level of logging
     * possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
     */
    configuration.logLevel = TEST_PROD ? config.LOG_WARN : config.LOG_INFO;
    config.set(configuration);
};
