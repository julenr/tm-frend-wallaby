"use strict";
const { root } = require('./helpers.js');

exports.PROTOCOL = 'https';
exports.HOST = 'preview.dev.trademe.co.nz';
exports.DEV_PORT = 5000;
exports.E2E_PORT = 4201;
exports.PROD_PORT = 8088;
exports.UNIVERSAL_PORT = 8000;

exports.AUTOPREFIXER_BROWSERS = [
    '> 5%',
    'Explorer 11',
    'Firefox >= 30',
    'Chrome >= 34',
    'Safari >= 5.1',
    'Opera >= 22',
    'Android >= 4.1'
];

exports.EXCLUDE_SOURCE_MAPS = [
    // these packages have problems with their sourcemaps
    root('node_modules/@angular'),
    root('node_modules/rxjs')
];

/**
 * specifies which @ngrx dev tools will be available when you build and load
 * your app in dev mode. Options are: monitor | logger | both | none
 */
exports.STORE_DEV_TOOLS = 'monitor';
