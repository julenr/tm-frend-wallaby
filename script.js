var fs = require('fs');

function rename(text) {
    return text.replace('.component.js', '.po.js');
}

function renameDir(dir) {
    var files = fs.readdirSync(dir),
        f,
        fileName,
        path,
        newPath,
        file;

    for (f = 0; f < files.length; f += 1) {
        fileName = files[f];
        path = dir + '/' + fileName;
        file = fs.statSync(path);
        newPath = dir + '/' + rename(fileName);
        fs.renameSync(path, newPath);
        if (file.isDirectory()) {
            renameDir(newPath);
        }
    }
}

renameDir(process.cwd());