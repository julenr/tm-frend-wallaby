const { createEventFilter, getArg, printBanner, yarnAdd } = require('./_build/build.config');

// Yarn add
// this will also yarn install

console.log('Installing sentry-cli with yarn...');
yarnAdd('sentry-cli-binary');
console.log('sentry-cli Installed');

const { Build } = require('@trademe/observabuild'); // require after yarn add
const { root } = require('./helpers');
const path = require('path');

// Post build

const eventFilter = createEventFilter(root('.'));

const OUT_DIR_FLAG = '--outdir=';
const HG_REV_FLAG = '--revisionid=';

const OUT_DIR = getArg(OUT_DIR_FLAG);
const HG_REV = getArg(HG_REV_FLAG);

let needsNewRelease = false;

new Build({ eventFilter: eventFilter })
    .do({ next: (task) => printBanner() })
    .do({ next: (task) => {
        if (!OUT_DIR) {
            task.error('Could not publish sourcemaps to sentry, no outDir was supplied to the postbuild script. Please pass in outDir with flag: ' + OUT_DIR_FLAG);
            return;
        }
        if (!HG_REV) {
            task.error('Could not publish sourcemaps to sentry, no hg revision was supplied to the postbuild script. Please pass in rev with flag: ' + HG_REV_FLAG);
            return;
        }
        task.done();
    }})
    .nodeBin({
        command: 'sentry-cli', args: ['releases', '-o', 'trade-me', 'info', HG_REV],
        statusMessage: { start: 'Checking for existing release...', fail: 'Sentry-cli: check current release: failed' },
        name: 'Check release',
        haltOnErrors: false, // sentry-cli returns exitCode 1 if 'No such release'
        response: (data) => {
            if (/No such release/i.test(data)) {
                needsNewRelease = true;
                return 'Sentry-cli: release not found';
            } else {
                return `Sentry-cli: found existing release for ${HG_REV}`;
            }
        }
    })
    .if(() => needsNewRelease, tasks => {
        tasks.nodeBin({
            command: 'sentry-cli', args: ['releases', '-o', 'trade-me', '-p', 'trademefrend', 'new', HG_REV],
            statusMessage: { start: 'Sentry-cli: creating new release...', fail: 'Sentry-cli: create new release: failed.', success: `Sentry-cli: created release ${HG_REV}` },
            name: 'Create release'
        });
        tasks.nodeBin({
            command: 'sentry-cli', args: ['releases', '-o', 'trade-me', '-p', 'trademefrend', 'files', HG_REV, 'upload-sourcemaps', path.normalize(OUT_DIR)],
            statusMessage: {
                start: `
Sentry-cli: uploading sourcemap files for release.
Sentry-cli: this may take some time...`,
                success: 'Sentry-cli: upload sourcemaps for release: success!',
                fail: 'Sentry-cli: upload sourcemaps for release: failed.'
            },
            name: 'Upload source maps'
        });
    })
    .do({ next: (task) => printSuccess() })
    .start();

function printSuccess() {
    return `
-----------------------------------------------------------
Done!
-----------------------------------------------------------`
}
