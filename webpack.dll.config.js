const {
    ContextReplacementPlugin,
    DefinePlugin,
    DllPlugin,
    ProgressPlugin,
    NamedModulesPlugin,
    NoEmitOnErrorsPlugin
} = require('webpack');
const AutoPrefixer = require('autoprefixer');
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader');

const { root, resolveToScss } = require('./helpers.js');
const { AUTOPREFIXER_BROWSERS, DEV_PORT, EXCLUDE_SOURCE_MAPS, HOST, STORE_DEV_TOOLS } = require('./constants');

const CONSTANTS = {
    AOT: false,
    ENV: JSON.stringify('development'),
    HMR: false,
    HOST: JSON.stringify(HOST),
    PORT: DEV_PORT,
    STORE_DEV_TOOLS: JSON.stringify(STORE_DEV_TOOLS),
    UNIVERSAL: false
};

module.exports = {
    resolve: {
        extensions: ['.ts', '.js', '.json'],
        alias: {
            '@trademe/core': root('src/app/core'),
            '@trademe/testing': root('test-config')
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                enforce: 'pre',
                loader: 'source-map-loader',
                exclude: EXCLUDE_SOURCE_MAPS
            },
            {
                test: /\.ts$/,
                loaders: [
                    '@angularclass/hmr-loader',
                    'awesome-typescript-loader?{configFileName: "tsconfig.webpack.json"}',
                    'angular2-template-loader',
                    'angular-router-loader?loader=system&aot=false'
                ],
                exclude: [/\.(spec|e2e|d)\.ts$/]
            },
            { test: /\.html/, loader: 'raw-loader', exclude: [root('src/index.html')] },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: 'to-string-loader', options: { sourceMap: true }
                    },
                    {
                        loader: 'css-loader',
                        options: { sourceMap: true, url: false }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            plugins: [AutoPrefixer({ browsers: AUTOPREFIXER_BROWSERS })]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            includePaths: [root('node_modules')],
                            importer: resolveToScss
                        }
                    }
                ],
            },
            { test: /\.svg/, loader: 'svg-inline-loader?removeSVGTagAttrs=false' }
        ]
    },
    plugins: [
        new ProgressPlugin(),
        new CheckerPlugin(),
        new DefinePlugin(CONSTANTS),
        new NamedModulesPlugin(),
        new ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, './src', {}),
        new TsConfigPathsPlugin(),
        new NoEmitOnErrorsPlugin(),
        new DllPlugin({
            name: '[name]',
            path: root('dll/[name]-manifest.json')
        })
    ],
    cache: true,
    devtool: 'cheap-module-eval-source-map',
    entry: {
        polyfill: [
            'sockjs-client',
            '@angularclass/hmr-loader',
            'tslib',
            'core-js/client/shim.js',
            'core-js/es6/reflect.js',
            'core-js/es7/reflect.js',
            'querystring-es3',
            'strip-ansi',
            'url',
            'punycode',
            'events',
            'webpack-dev-server/client/socket.js',
            'webpack/hot/emitter.js',
            'zone.js/dist/zone.js',
            'zone.js/dist/zone-error.js',
            'zone.js/dist/long-stack-trace-zone.js',
        ],
        vendor: [
            '@angular/animations',
            '@angular/common',
            '@angular/compiler',
            '@angular/core',
            '@angular/forms',
            '@angular/http',
            '@angular/platform-browser',
            '@angular/platform-browser-dynamic',
            '@angular/platform-server',
            '@angular/router',
            '@ngrx/effects',
            '@ngrx/store',
            '@ngrx/store-devtools',
            'ngrx-store-freeze',
            'ngrx-store-logger',
            'rxjs',
            '@trademe/tangram',
            'angular-2-local-storage',
            'ngx-cookie',
            'core-js',
            'deep-equal',
            'mailcheck',
            'reselect',
            'svg4everybody'
        ]
    },
    output: {
        path: root('dll'),
        filename: '[name].dll.js',
        library: '[name]'
    },
    performance: {
        hints: false
    },
    node: {
        global: true,
        process: true,
        Buffer: false,
        crypto: true,
        module: false,
        clearImmediate: false,
        setImmediate: false,
        clearTimeout: true,
        setTimeout: true
    },
    stats: {
        assets: true,
        children: false,
        chunks: false,
        chunksModules: false,
        chunkOrigins: false,
        maxModules: 0 // hide list of built modules
    }
};
