const {
    createEventFilter,
    getArg,
    printBanner,
    printSuccess,
    STATUS_MESSAGES,
    TEST_EVENT_FILTER,
    yarnInstall
} = require('./_build/build.config');

// yarn install

yarnInstall();

// Build

const { Build } = require('@trademe/observabuild'); // require after yarn install
const { root } = require('./helpers');

const rootPath = root('.');
const eventFilter = createEventFilter(rootPath);

let outDir = getArg('--outdir=');
if (outDir) {
    console.log(`outdir=${outDir}`);
}

new Build({ eventFilter: eventFilter })
    .do({ next: (task) => printBanner() })
    .parallel(tasks => {
        tasks.nodeBin({
            command: 'rimraf', args: ['dist .awcache reports'],
            name: 'Clean Folders', prefix: 'Clean', flowId: 'teamcityclean'
        });
        tasks.do({
            next: (task) => task.securityCheck(rootPath),
            name: 'Node Security Check', prefix: 'Check', flowId: 'teamcitysecuritycheck'
        });
    })
    .parallel(tasks => {
        tasks.yarn({
            command: 'lint',
            name: 'Lint', prefix: 'Lint', flowId: 'teamcitylint', statusMessage: STATUS_MESSAGES.lint
        });
        tasks.serial(testTasks => {
            testTasks
                .yarn({
                    command: 'test:prod',
                    name: 'Unit Tests', prefix: 'Test', flowId: 'teamcitytest', statusMessage: STATUS_MESSAGES.test,
                    eventFilter: TEST_EVENT_FILTER
                })
                .do({
                    next: (task) => task.publishArtifact(root('./reports/coverage/html'), root('./reports/coverage.zip')),
                    name: 'Coverage Report', prefix: 'Cover', flowId: 'teamcitytest', statusMessage: STATUS_MESSAGES.coverage
                });
        });
        tasks.serial(buildTasks => {
            buildTasks
                .yarn({
                    command: 'build:prod',
                    name: 'Production Build', prefix: 'Build', flowId: 'teamcitywebpack', statusMessage: STATUS_MESSAGES.build
                })
                .do({
                    next: (task) => task.publishArtifact(root('./reports/bundles'), root('./reports/bundles.zip')),
                    name: 'Bundle Report', prefix: 'Bundle', flowId: 'teamcitywebpack', statusMessage: STATUS_MESSAGES.bundle
                });
        });
    })
    .do({ next: (task) => task.copyFolder(root('./dist/client'), outDir) })
    .do({ next: (task) => printSuccess(outDir) })
    .start();
